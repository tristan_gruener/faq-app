import React, { Component } from 'react'
import Header from './components/Header'
import SideNav from './components/SideNav/SideNav'
import AddFaq from './components/AddFaq'
import AddProduct from './components/AddProduct'
import Home from './components/Home'
import ProductPage from './components/ProductPage'
import AddBulk from './components/AddBulk'
import AddIngredient from './components/AddIngredient'
import AddNordicStandard from './components/AddNordicStandard'
import BulkPage from './components/BulkPage'
import IngredientPage from './components/IngredientPage'
import NordicStandardPage from './components/NordicStandardPage'
import Login from './components/Login'
import Admin from './components/Admin'
import RecentlyModified from './components/RecentlyModified'
import { BrowserRouter, Route } from 'react-router-dom'
import './css/style.css'

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <div className="row">
            <Header/>
          </div>
          <div className="row">
            <div className="col m3 s12">
              <SideNav/>
            </div>
            <div className="col m9 s12">
              <Route path="/faqs" component={Home} exact={true}/>
              <Route path="/faqs/login" component={Login}/>
              <Route path="/faqs/admin" component={Admin}/>
              <Route path="/faqs/add-faq" component={AddFaq}/>
              <Route path="/faqs/add-product" component={AddProduct}/>
              <Route path="/faqs/add-bulk" component={AddBulk}/>
              <Route path="/faqs/add-ingredient" component={AddIngredient}/>
              <Route path="/faqs/add-nordic-standard" component={AddNordicStandard}/>
              <Route path="/faqs/ingredient/:ingredientId" component={IngredientPage}/>
              <Route path="/faqs/product/:productId" component={ProductPage}/>
              <Route path="/faqs/bulk/:bulkId" component={BulkPage}/>
              <Route path="/faqs/nordic-standard/:standardId" component={NordicStandardPage}/>
            </div>

          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
