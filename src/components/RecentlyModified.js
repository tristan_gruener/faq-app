import React from 'react'
import axios from 'axios'
import moment from 'moment'
import { Link } from 'react-router-dom'


// const RecentlyModified = () => (
class RecentlyModified extends React.Component {
  constructor(){
    super()
    this.getRecentlyModified()

    this.state = {
      recentlyModified: []
    }
  }

  getRecentlyModified() {
    // axios.get('https://localhost:4430/faq/rec-mod/')
    axios.get('/faq/rec-mod/')
      .then(res => {
        this.setState({
          recentlyModified: res.data
        }, () => {
          console.log(this.state.recentlyModified)
        })
      })
  }

  render() {

    return(
      <div id="recently-modified">
        {/* <h5 className="latest-updates-title">Latest Updates</h5> */}
        <b>Recently Modified</b>

          {this.state.recentlyModified.map((recMod) => {
            let type = recMod.url.split('/')[2]
            // todo: add all the different type conversions here!!
            switch (type){
              case 'standard':
                type = 'nordic-standard'
                break;
            }

            const id = recMod.url.split('/')[3]
            const url = `/faqs/${type}/${id}`
            // <p key={recMod.name}><Link to={url}></Link>

            const nsName = recMod.name.split(':')
            const ns = recMod.date_modified.split('-')
            const year = ns[0]
            const month = ns[1]
            const day = ns[2].split('T')[0]
            return (
              <Link to={url}>
                <div class="rec-mod-item">
                  <p>{nsName[0]}: <span className="rec-mod-link">{nsName[1]}</span></p>
                  <i>{month} / {day} / {year}</i>
                </div>
              </Link>
            )
          })}
      </div>
    )
  }
}

export default RecentlyModified
