import React from 'react'
import { Link } from 'react-router-dom'
import FaqList from './FaqList'

class IngredientPage extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      name: 'placeholder',
      renderBulksContainingIngredient: false,
      bulksContainingIngredient: [],
      productsContainingIngredient: [],
      matchingProds: [],
      bulksInIngredient: []
    }
  }

  componentDidMount() {
    this.init()
  }

  componentDidUpdate(prevProps, prevState, snapshot){
    if(this.props.match.params.ingredientId !== prevProps.match.params.ingredientId){
      this.init()
    }
  }

  init(){
    let ingredientHasInfoPage = false
    let needsInfoPageOnEdit = false
    let infoPageId = ''
    const urlId = this.props.location.pathname.split('/')[3]

    fetch(`/faq/ingredient/${urlId}/`).then((res) => res.json()).then((json) => {
    // fetch(`https://localhost:4430/faq/ingredient/${urlId}/`).then((res) => res.json()).then((json) => {
      const ingredientInfo = JSON.stringify(json)
      this.getProductsContainingIngredient(urlId)
      this.getBulksContainingIngredient()
      //check if info page is present
      try {
        infoPageId = JSON.parse(ingredientInfo).info_pages[0].id
        ingredientHasInfoPage = true
      } catch(error){
        needsInfoPageOnEdit = true
      }

      if(ingredientHasInfoPage) {
        const infoPages = JSON.parse(JSON.parse(ingredientInfo).info_pages[0].html_j)
        const generalInformation = infoPages.generalInformation
        const sourcing = infoPages.sourcing
        const healthInformation = infoPages.healthInformation
        const allergens = infoPages.allergens

        this.setState({
          description: json.description,
          id: json.id,
          infoPageId: infoPageId,
          needsInfoPageOnEdit: needsInfoPageOnEdit,
          labelDeclaration: generalInformation.labelDeclaration,
          functions: generalInformation.functions,
          dailyValue: generalInformation.dailyValue,
          recommendedDailyIntake: generalInformation.recommendedDailyIntake,
          differentFormsOfIngredient: generalInformation.differentFormsOfIngredient,
          countryOfOrigin: sourcing.countryOfOrigin,
          naturalFoodSources: sourcing.naturalFoodSources,
          deficiancies: healthInformation.deficiancies,
          contraindications: '',
          medicationInteractions: healthInformation.medicationInteractions,
          toxicitySafety: healthInformation.toxicitySafety,
          manufactureWarning: parseInt(allergens.manufactureWarning),
          manufactureWarningRender: this.setManufactureWarning(parseInt(allergens.manufactureWarning)),
          milkAllergen: allergens.milkAllergen,
          eggAllergen: allergens.eggAllergen,
          soyAllergen: allergens.soyAllergen,
          fishAllergen: allergens.fishAllergen,
          shellFishAllergen: allergens.shellFishAllergen,
          wheatAllergen: allergens.wheatAllergen,
          treeNutsAllergen: allergens.treeNutsAllergen,
          peanutsAllergen: allergens.peanutsAllergen,
          faqs: json.faqs,
          published: json.published,
          active: json.active,
          featured: json.featured,
          featuredHeadline: json.featured_headline
        })
      } else {
        this.setState({
          description: json.description,
          id: json.id,
          infoPageId: '',
          needsInfoPageOnEdit: needsInfoPageOnEdit,
          labelDeclaration: '',
          functions: '',
          dailyValue: '',
          recommendedDailyIntake: '',
          differentFormsOfIngredient: '',
          countryOfOrigin: '',
          naturalFoodSources: '',
          deficiancies: '',
          contraindications: '',
          medicationInteractions: '',
          toxicitySafety: '',
          manufactureWarning: 0,
          milkAllergen: false,
          eggAllergen: false,
          soyAllergen: false,
          fishAllergen: false,
          shellFishAllergen: false,
          wheatAllergen: false,
          treeNutsAllergen: false,
          peanutsAllergen: false,
          faqs: [],
          featured: false,
          featuredHeadline: ''
        })
      }
    })
    window.scrollTo(0,0)
  }

  setManufactureWarning(val) {
    var wrng = ''
    switch (val){
      case 0:
        wrng =
          <span style={{ "color":"#4caf50" }}>
            <i className="fas fa-thumbs-up"></i> This product is manufactured in a facility free of Soy, Dairy, Wheat, Rye, Barley, and Nuts
          </span>
        break;
      case 1:
        wrng =
          <span style={{ "color":"#d81b60" }}>
            <i className="fas fa-exclamation-triangle"></i> This product is manufactured in a facility that processes Soy, Dairy, Wheat, Rye, Barley, or Nuts
          </span>
        break;
      case 2:
        wrng = ''
    }
    return wrng
  }

  getBulksContainingIngredient() {
    const bulksInIngredient = []
    const publishedBulks = []
    // fetch(`https://localhost:4430/faq/bulk/`).then((res) => res.json()).then((json) => {
    fetch(`/faq/bulk/`).then((res) => res.json()).then((json) => {
      json.map((bulk) => {
        if(bulk.published){
          publishedBulks.push(bulk)
        }
      })

      //now loop through these bulks and plug them into the vending machine
      publishedBulks.map((bulk) => {
        const temp = localStorage.getItem(`bulkWithIngredient-${bulk.id}`)
        const ingredientsOfThisBulk = JSON.parse(temp)
        if(ingredientsOfThisBulk !== null){
          ingredientsOfThisBulk.map((ingredient) => {
            const localIngredientId = parseInt(ingredient.split('/')[5])
            if(localIngredientId === this.state.id){
              bulksInIngredient.push(bulk)
            }
          })
        }
      })
      this.setState({ bulksInIngredient }, () => {
        console.log(this.state.bulksInIngredient)
      })
    })
  }

  getProductsContainingIngredient(ingredientId) {
      //ALSO MAKE SURE IT DOESN'T CRASH IF THERE ARE NO MATCHES!!
      //ALSO MAKE SURE IT DOESN'T CRASH IF THERE ARE NO MATCHES!!
      //ALSO MAKE SURE IT DOESN'T CRASH IF THERE ARE NO MATCHES!!
      //ALSO MAKE SURE IT DOESN'T CRASH IF THERE ARE NO MATCHES!!

      let publishedProducts = []
      // fetch(`https://localhost:4430/faq/product/`).then((res) => res.json()).then((json) => {
      fetch(`/faq/product/`).then((res) => res.json()).then((json) => {
        json.map((product) => {
          if(product.published){
            publishedProducts.push(product)
          }
        })
        const matchingProds = []
        publishedProducts.map((prod) => {
          //then with each product id you can enter it into the first vending machine
          // --> localStorage('product-444', 'bulkInProduct')
          const bulkOfThisProd = localStorage.getItem(`product-${prod.id}`)
          //then you enter that result (the bulk) into the next vending machine
          // --> localStorage('bulk-888', 'ingredientsInBulk')
          const temp = localStorage.getItem(`bulkWithIngredient-${bulkOfThisProd.split('/')[5]}`)
          const ingredientsOfThisBulk = JSON.parse(temp)
          //it could be empty on some, i bet im going out of bounds on an array here
          if(ingredientsOfThisBulk !== null){
            ingredientsOfThisBulk.map((ingredient) => {
              const localIngredientId = ingredient.split('/')[5]
              //with the ingredientsInBulk you will be able to loop through them and determine if its the same as the ingredient you are currently on
              //because if it is that means the product id you entered into the vending machine should be in the list
              if(localIngredientId === ingredientId){
                matchingProds.push(prod)
              }
            })
          }
        })
        this.setState({ matchingProds })
      })
  }

  render(){
    return(
      <div id="ingredient-page">
        <div>
          <h2>{this.state.description}</h2>

          <p><b>Label Declaration</b>: <span className="item-number">{this.state.labelDeclaration}</span></p>

          <h4>General Information</h4>

          <p><b>Description:</b> {this.state.functions}</p>
          <p><b>Daily Value:</b> {this.state.dailyValue}</p>
          {/* <p><b>Recommended Daily Intake / Allowance</b> {this.state.recommendedDailyIntake}</p> */}
          <p><b>Different forms of Ingredient</b> {this.state.differentFormsOfIngredient}</p>

          <h4>Sourcing</h4>

          <p><b>Country of Origin</b> {this.state.countryOfOrigin}</p>
          <p><b>Source</b> {this.state.naturalFoodSources}</p>

          <h4>Health Information</h4>

          <p><b>Deficiancies</b> {this.state.deficiancies}</p>
          <p><b><i className="fas fa-briefcase-medical"></i> Contraindications / Medication Interactions</b> {this.state.medicationInteractions}</p>
          <p><b><i className="fas fa-briefcase-medical"></i> Toxicity Safety</b> {this.state.toxicitySafety}</p>

          <h4>Bulks Containing Ingredient</h4>

          {this.state.bulksInIngredient.map((bulk) => (
            <p key={bulk.id}><Link to={`/faqs/bulk/${bulk.id}/`}>{bulk.name}</Link></p>
          ))}

          <h4>Products Containing Ingredient</h4>

          {this.state.matchingProds.map((product) => (
            <p key={product.id}><Link to={`/faqs/product/${product.id}/`}>{product.description}</Link></p>
          ))}

          <h4>Allergens</h4>

          <b>{this.state.manufactureWarningRender}</b>

          <table>
            <thead>
              <tr>
                <th>Milk</th>
                <th>Eggs</th>
                <th>Soy</th>
                <th>Fish</th>
                <th>Shellfish</th>
                <th>Wheat</th>
                <th>Treenuts</th>
                <th>Peanuts</th>
              </tr>
            </thead>

            <tbody>
              <tr>
                <td>{this.state.milkAllergen && <i className="fas fa-check"></i>}</td>
                <td>{this.state.eggAllergen && <i className="fas fa-check"></i>}</td>
                <td>{this.state.soyAllergen && <i className="fas fa-check"></i>}</td>
                <td>{this.state.fishAllergen && <i className="fas fa-check"></i>}</td>
                <td>{this.state.shellFishAllergen && <i className="fas fa-check"></i>}</td>
                <td>{this.state.wheatAllergen && <i className="fas fa-check"></i>}</td>
                <td>{this.state.treeNutsAllergen && <i className="fas fa-check"></i>}</td>
                <td>{this.state.peanutsAllergen && <i className="fas fa-check"></i>}</td>
              </tr>
            </tbody>
          </table>
          {localStorage.getItem('isLoggedIn') === "true" &&
            <Link
              to={{
                pathname: "/faqs/add-ingredient",
                state: {
                  category: "Allergens",
                  context: "Edit",
                  needsInfoPageOnEdit: this.state.needsInfoPageOnEdit,
                  id: this.state.id,
                  description: this.state.description,
                  nameRef: this.state.description,
                  labelDeclaration: this.state.labelDeclaration,
                  infoPageId: this.state.infoPageId,
                  functions: this.state.functions,
                  dailyValue: this.state.dailyValue,
                  recommendedDailyIntake: this.state.recommendedDailyIntake,
                  differentFormsOfIngredient: this.state.differentFormsOfIngredient,
                  countryOfOrigin: this.state.countryOfOrigin,
                  naturalFoodSources: this.state.naturalFoodSources,
                  deficiancies: this.state.deficiancies,
                  contraindications: this.state.contraindications,
                  medicationInteractions: this.state.medicationInteractions,
                  toxicitySafety: this.state.toxicitySafety,
                  active: this.state.active,
                  manufactureWarning: this.state.manufactureWarning,
                  milkAllergen: this.state.milkAllergen,
                  eggAllergen: this.state.eggAllergen,
                  soyAllergen: this.state.soyAllergen,
                  fishAllergen: this.state.fishAllergen,
                  shellFishAllergen: this.state.shellFishAllergen,
                  wheatAllergen: this.state.wheatAllergen,
                  treeNutsAllergen: this.state.treeNutsAllergen,
                  peanutsAllergen: this.state.peanutsAllergen,
                  ingredientType: this.state.ingredientType,
                  published: this.state.published,
                  featured: this.state.featured,
                  featuredHeadline: this.state.featuredHeadline
              }}}
            >
              <span className="waves-effect waves-light btn right">Edit Raw Material</span>
            </Link>
          }

          <div className="section-heading">
            <h4>FAQs</h4>
            {this.state.faqs !== undefined &&
              <FaqList
                faqs={this.state.faqs}
                pastRef={this.props.location.pathname}
                nameRef={this.state.name}
                prodIdRef={this.state.id}
              />
            }
            {localStorage.getItem('isLoggedIn') === "true" &&
              <Link
                to={{
                  pathname: "/faqs/add-faq",
                  state: {
                    category: "generalInformation",
                    objectType: "ingredient",
                    nameRef: this.state.description,
                    pastRef: this.props.location.pathname,
                    prodIdRef: this.state.id
                  }
                }}
              >
               <span className="waves-effect waves-light btn right">Add Faq</span>
              </Link>
            }
          </div>
        </div>
      </div>
    )
  }
}

export default IngredientPage
