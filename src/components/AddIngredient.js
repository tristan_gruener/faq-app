import React from 'react'
import { Input } from 'react-materialize'
import _ from 'lodash'

class AddIngredient extends React.Component {
  constructor(props){
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.updateManufactureWarning = this.updateManufactureWarning.bind(this)
    this.updateMilkAllergen = this.updateMilkAllergen.bind(this)
    this.updateEggAllergen = this.updateEggAllergen.bind(this)
    this.updateSoyAllergen = this.updateSoyAllergen.bind(this)
    this.updateFishAllergen = this.updateFishAllergen.bind(this)
    this.updateShellFishAllergen = this.updateShellFishAllergen.bind(this)
    this.updateWheatAllergen = this.updateWheatAllergen.bind(this)
    this.updateTreeNutsAllergen = this.updateTreeNutsAllergen.bind(this)
    this.updatePeanutsAllergen = this.updatePeanutsAllergen.bind(this)

    // ----
    // ADD
    // ----
    if(_.isEmpty(this.props.location.state)){
      this.state = {
        context: 'Add',
        ingredientDescription: '',
        labelDeclaration: '',
        functions: '',
        dailyValue: '',
        recommendedDailyIntake: '',
        differentFormsOfIngredient: '',
        countryOfOrigin: '',
        naturalFoodSources: '',
        deficiancies: '',
        contraindications: '',
        medicationInteractions: '',
        toxicitySafety: '',
        manufactureWarning: false,
        milkAllergen: false,
        eggAllergen: false,
        soyAllergen: false,
        fishAllergen: false,
        shellFishAllergen: false,
        wheatAllergen: false,
        treeNutsAllergen: false,
        peanutsAllergen: false,
        featured: false,
        featuredHeadline: ''
      }
    }else{
      // ----
      // EDIT
      // ----
      this.state = {
        context: this.props.location.state.context,
        ingredientId: this.props.location.state.id,
        infoPageId: this.props.location.state.infoPageId,
        needsInfoPageOnEdit: this.props.location.state.needsInfoPageOnEdit,
        ingredientDescription: this.props.location.state.description,
        labelDeclaration: this.props.location.state.labelDeclaration,
        functions: this.props.location.state.functions,
        dailyValue: this.props.location.state.dailyValue,
        recommendedDailyIntake: this.props.location.state.recommendedDailyIntake,
        differentFormsOfIngredient: this.props.location.state.differentFormsOfIngredient,
        countryOfOrigin: this.props.location.state.countryOfOrigin,
        naturalFoodSources: this.props.location.state.naturalFoodSources,
        deficiancies: this.props.location.state.deficiancies,
        contraindications: '',
        medicationInteractions: this.props.location.state.medicationInteractions,
        toxicitySafety: this.props.location.state.toxicitySafety,
        manufactureWarning: this.props.location.state.manufactureWarning,
        milkAllergen: this.props.location.state.milkAllergen,
        eggAllergen: this.props.location.state.eggAllergen,
        soyAllergen: this.props.location.state.soyAllergen,
        fishAllergen: this.props.location.state.fishAllergen,
        shellFishAllergen: this.props.location.state.shellFishAllergen,
        wheatAllergen: this.props.location.state.wheatAllergen,
        treeNutsAllergen: this.props.location.state.treeNutsAllergen,
        peanutsAllergen: this.props.location.state.peanutsAllergen,
        published: this.props.location.state.published === true ? 1 : 0,
        active: this.props.location.state.active === true ? 1 : 0,
        featured: this.props.location.state.featured,
        featuredHeadline: this.props.location.state.featuredHeadline
      }
    }
  }

  handleSubmit(e){
    e.preventDefault()
    const ingredientDescription = e.target.elements.ingredientDescription.value
    const published = e.target.elements.published.value
    const active = e.target.elements.active.value
    const labelDeclaration = e.target.elements.labelDeclaration.value
    const functions = e.target.elements.functions.value
    const dailyValue = e.target.elements.dailyValue.value
    const recommendedDailyIntake = e.target.elements.recommendedDailyIntake.value
    const differentFormsOfIngredient = e.target.elements.differentFormsOfIngredient.value
    const countryOfOrigin = e.target.elements.countryOfOrigin.value
    const naturalFoodSources = e.target.elements.naturalFoodSources.value
    const deficiancies = e.target.elements.deficiancies.value
    const medicationInteractions = e.target.elements.medicationInteractions.value
    const toxicitySafety = e.target.elements.toxicitySafety.value
    const manufactureWarning = e.target.elements.manufactureWarning.value
    const milkAllergen = this.state.milkAllergen
    const eggAllergen = this.state.eggAllergen
    const soyAllergen = this.state.soyAllergen
    const fishAllergen = this.state.fishAllergen
    const shellFishAllergen = this.state.shellFishAllergen
    const wheatAllergen = this.state.wheatAllergen
    const treeNutsAllergen = this.state.treeNutsAllergen
    const peanutsAllergen = this.state.peanutsAllergen

    const featured = this.state.featured
    const featuredHeadline = this.state.featuredHeadline

    let url = '/faq/ingredient/'
    let method = ''
    let data = {}
    const infoPageData = {
      generalInformation: {
        'labelDeclaration': labelDeclaration,
        'functions': functions,
        'dailyValue': dailyValue,
        'recommendedDailyIntake': recommendedDailyIntake,
        'differentFormsOfIngredient': differentFormsOfIngredient
      },
      sourcing: {
        'countryOfOrigin': countryOfOrigin,
        'naturalFoodSources': naturalFoodSources
      },
      healthInformation : {
        'deficiancies': deficiancies,
        'medicationInteractions': medicationInteractions,
        'toxicitySafety': toxicitySafety
      },
      allergens: {
        'manufactureWarning': manufactureWarning,
        'milkAllergen': milkAllergen,
        'eggAllergen': eggAllergen,
        'soyAllergen': soyAllergen,
        'fishAllergen': fishAllergen,
        'shellFishAllergen': shellFishAllergen,
        'wheatAllergen': wheatAllergen,
        'treeNutsAllergen': treeNutsAllergen,
        'peanutsAllergen': peanutsAllergen
      }
    }

    switch (this.state.context) {
      case 'Add':
        method = 'POST'
        // ----
        // NEW ingredient PAGE
        // ----
        data = {
          'description': ingredientDescription,
          'published': published,
          'active': active,
          'faqs': [],
          'info_pages': [{
            'html_j': JSON.stringify(infoPageData)
          }],
          'featured': featured,
          'featured_headline': featuredHeadline
        }
        this.pushToDb(url, method, data, 'add')
        break;

      case 'Edit':
        method = 'PUT'
        if (this.state.needsInfoPageOnEdit) {
          // ----
          // EDIT xTuple ingredient PAGE
          // ----
          data = {
            'description': ingredientDescription,
            'published': published,
            'active': active,
            'faqs': [],
            'info_pages': [{
              'html_j': JSON.stringify(infoPageData)
            }],
            'featured': featured,
            'featured_headline': featuredHeadline
          }
        } else {
          // ----
          // EDIT NATIVE ingredient PAGE
          // ----
          data = {
            'description': ingredientDescription,
            'published': published,
            'active': active,
            'faqs': [],
            'info_pages': [],
            'featured': featured,
            'featured_headline': featuredHeadline
          }
        }
        url = `/faq/ingredient/${this.state.ingredientId}/`
        this.pushToDb(url, method, data, 'edit', 'step-1')

        // ----
        // EDIT NATIVE INFO PAGE
        // ----
        data = {
          'html_j': JSON.stringify(infoPageData)
        }
        url = `/faq/info-page/${this.state.infoPageId}/`
        this.pushToDb(url, method, data, 'edit', 'step-2')
        break;
    }
  }

  updateIngredientList() {
    console.log('update ingredient list')
  }

  pushToDb(url, method, data, delay, step){
    fetch(url, {
      method: method,
      body: JSON.stringify(data),
      headers: {
        'Authorization': ' Token ' + localStorage.getItem('token'),
        'Content-Type': 'application/json'
      }
    }).then(res => res.json()).then(response => {
      if(delay === 'add') {
        alert('succesfully added item')
        const url = `/faqs/ingredient/${response.id}/`
        this.props.history.push(url)
        window.scrollTo(0,0)
      }

      if(delay === 'edit') {
        if(step === 'step-1'){
          alert('successfully edited item')
          const url = `/faqs/ingredient/${response.id}/`
          this.props.history.push(url)
          window.scrollTo(0,0)
        }
      }
    })
  }

  handleDelete(){
    const url = `/faq/ingredient/${this.state.ingredientId}/`
    const deleteConfirmation = window.confirm("Are you sure you want to delete this ingredient? (all the faqs that have been added will also be deleted)");
    if (deleteConfirmation) {
      fetch(url, {
        method: 'DELETE',
        headers: {
          'Authorization': ' Token ' + localStorage.getItem('token'),
          'Content-Type': 'application/json'
        }
      }).then(() => {
        window.location.assign('/faqs/')
      })
    }
  }

  updateIngredientDescription(evt){
    this.setState({
      ingredientDescription: evt.target.value
    });
  }

  updatePublished(evt){
    this.setState({
      published: evt.target.value
    });
  }

  updateActive(evt){
    this.setState({
      active: evt.target.value
    });
  }

  updateLabelDeclaration(evt){
    this.setState({
      labelDeclaration: evt.target.value
    })
  }

  updateFunctions(evt){
    this.setState({
      functions: evt.target.value
    })
  }

  updateDailyValue(evt){
    this.setState({
      dailyValue: evt.target.value
    })
  }

  updateRecommendedDailyIntake(evt){
    this.setState({
      recommendedDailyIntake: evt.target.value
    })
  }

  updateDifferentFormsOfIngredient(evt){
    this.setState({
      differentFormsOfIngredient: evt.target.value
    })
  }

  updateCountryOfOrigin(evt){
    this.setState({
      countryOfOrigin: evt.target.value
    })
  }

  updateNaturalFoodSources(evt){
    this.setState({
      naturalFoodSources: evt.target.value
    })
  }

  updateDeficiancies(evt){
    this.setState({
      deficiancies: evt.target.value
    })
  }

  updateContraindications(evt){
    this.setState({
      contraindications: evt.target.value
    })
  }

  updateMedicationInteractions(evt){
    this.setState({
      medicationInteractions: evt.target.value
    })
  }

  updateToxicitySafety(evt){
    this.setState({
      toxicitySafety: evt.target.value
    })
  }

  updateManufactureWarning(evt){
    this.setState({
      manufactureWarning: evt.target.value
    })
  }

  updateMilkAllergen(evt){
    this.setState({
      milkAllergen: !this.state.milkAllergen
    })
  }

  updateEggAllergen(evt){
    this.setState({
      eggAllergen: !this.state.eggAllergen
    })
  }

  updateSoyAllergen(evt){
    this.setState({
      soyAllergen: !this.state.soyAllergen
    })
  }

  updateFishAllergen(evt){
    this.setState({
      fishAllergen: !this.state.fishAllergen
    })
  }

  updateShellFishAllergen(evt){
    this.setState({
      shellFishAllergen: !this.state.shellFishAllergen
    })
  }

  updateWheatAllergen(evt){
    this.setState({
      wheatAllergen: !this.state.wheatAllergen
    })
  }

  updateTreeNutsAllergen(evt){
    this.setState({
      treeNutsAllergen: !this.state.treeNutsAllergen
    })
  }

  updatePeanutsAllergen(evt){
    this.setState({
      peanutsAllergen: !this.state.peanutsAllergen
    })
  }

  updateFeatured(evt){
    this.setState({
      featured: evt.target.value
    });
  }

  updateFeaturedHeadline(evt){
    this.setState({
      featuredHeadline: evt.target.value
    });
  }

  render(){
    return(
      <div>
        <h2>{this.state.context === 'Edit' ? `Editing ${this.state.ingredientDescription}` : 'Add Raw Material'}</h2>
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="input-field col s10">
              <input
                id="ingredient-description"
                type="text"
                className="validate"
                name="ingredientDescription"
                value={this.state.ingredientDescription}
                onChange={evt => this.updateIngredientDescription(evt)}
              />
              <label htmlFor="ingredient-description" className={this.state.context === 'Edit' && 'active'}>Raw Material Name</label>
            </div>
          </div>

          <h4>General Information</h4>

          <div className="row">
            <div className="input-field col s5">
              <input
                id="label-declaration"
                type="text"
                className="validate"
                name="labelDeclaration"
                value={this.state.labelDeclaration}
                onChange={evt => this.updateLabelDeclaration(evt)}
              />
              <label htmlFor="label-declaration" className={this.state.context === 'Edit' && 'active'}>Label Declaration</label>
            </div>
          </div>

          <div className="row">
            <div className="input-field col s5">
              <input
                id="functions"
                type="text"
                className="validate"
                name="functions"
                value={this.state.functions}
                onChange={evt => this.updateFunctions(evt)}
              />
              <label htmlFor="functions" className={this.state.context === 'Edit' && 'active'}>Description</label>
            </div>
          </div>

          <div className="row">
            <div className="input-field col s5">
              <input
                id="daily-value"
                type="text"
                className="validate"
                name="dailyValue"
                value={this.state.dailyValue}
                onChange={evt => this.updateDailyValue(evt)}
              />
              <label htmlFor="daily-value" className={this.state.context === 'Edit' && 'active'}>Daily Value</label>
            </div>
          </div>

          <div className="row">
            <div className="input-field col s5">
              <input
                id="recommended-daily-intake"
                type="text"
                className="validate"
                name="recommendedDailyIntake"
                value={this.state.recommendedDailyIntake}
                onChange={evt => this.updateRecommendedDailyIntake(evt)}
              />
              <label htmlFor="recommended-daily-intake" className={this.state.context === 'Edit' && 'active'}>Recommended Daily Intake</label>
            </div>
          </div>

          <div className="row">
            <div className="input-field col s5">
              <input
                id="different-forms-of-ingredient"
                type="text"
                className="validate"
                name="differentFormsOfIngredient"
                value={this.state.differentFormsOfIngredient}
                onChange={evt => this.updateDifferentFormsOfIngredient(evt)}
              />
              <label htmlFor="different-forms-of-ingredient" className={this.state.context === 'Edit' && 'active'}>Different Forms of Ingredient </label>
            </div>
          </div>

          <h4>Sourcing</h4>

          <div className="row">
            <div className="input-field col s7">
              <input
                id="country-of-origin"
                type="text"
                className="validate"
                name="countryOfOrigin"
                value={this.state.countryOfOrigin}
                onChange={evt => this.updateCountryOfOrigin(evt)}
              />
              <label htmlFor="country-of-origin" className={this.state.context === 'Edit' && 'active'}>Country Of Origin </label>
            </div>
          </div>

          <div className="row">
            <div className="input-field col s7">
              <input
                id="natural-food-sources"
                type="text"
                className="validate"
                name="naturalFoodSources"
                value={this.state.naturalFoodSources}
                onChange={evt => this.updateNaturalFoodSources(evt)}
              />
              <label htmlFor="natural-food-sources" className={this.state.context === 'Edit' && 'active'}>Natural Food Sources </label>
            </div>
          </div>

          <h4>Health Information</h4>

          <div className="row">
            <div className="input-field col s7">
              <input
                id="deficiancies"
                type="text"
                className="validate"
                name="deficiancies"
                value={this.state.deficiancies}
                onChange={evt => this.updateDeficiancies(evt)}
              />
              <label htmlFor="deficiancies" className={this.state.context === 'Edit' && 'active'}>Deficiancies </label>
            </div>
          </div>

          <div className="row">
            <div className="input-field col s7">
              <input
                id="medication-interactions"
                type="text"
                className="validate"
                name="medicationInteractions"
                value={this.state.medicationInteractions}
                onChange={evt => this.updateMedicationInteractions(evt)}
              />
              <label htmlFor="medication-interactions" className={this.state.context === 'Edit' && 'active'}>Contraindications / Medication Interactions </label>
            </div>
          </div>

          <div className="row">
            <div className="input-field col s7">
              <input
                id="toxicity-safety"
                type="text"
                className="validate"
                name="toxicitySafety"
                value={this.state.toxicitySafety}
                onChange={evt => this.updateToxicitySafety(evt)}
              />
              <label htmlFor="toxicity-safety" className={this.state.context === 'Edit' && 'active'}>Toxicity / Safety </label>
            </div>
          </div>

          <h4>Allergens</h4>

          <div className="row">
            <div className="input-field">
              <div className="col s6">
                <p><b>Is this product manufactured in a facility that processes soy, dairy, wheat, rye, or barley?</b></p>
              </div>
              <div className="col s4" style={{"paddingTop": "14px"}}>

                <label style={{"marginRight": "14px"}}>
                  <input name="manufactureWarning" type="radio" value={1} onClick={this.updateManufactureWarning} defaultChecked={this.state.manufactureWarning === 1 && true}
                  />
                  <span>Yes</span>
                </label>

                <label style={{"marginRight": "14px"}}>
                  <input name="manufactureWarning" type="radio" value={0} onClick={this.updateManufactureWarning} defaultChecked={this.state.manufactureWarning === 0 && true} />
                  <span>No</span>
                </label>

                <label>
                  <input name="manufactureWarning" type="radio" value={2} onClick={this.updateManufactureWarning} defaultChecked={this.state.manufactureWarning === 2 && true}
                  />
                  <span>N/A</span>
                </label>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="input-field">
              <div className="col s8">
                <p><b>Specific Allergens</b></p>
                <label>
                  <input
                    type="checkbox"
                    name="milkAllergen"
                    defaultChecked={this.state.milkAllergen}
                    onChange={this.updateMilkAllergen}
                  />
                  <span>Milk</span>
                </label>
                <br />

                <label>
                  <input
                    type="checkbox"
                    name="eggAllergen"
                    defaultChecked={this.state.eggAllergen}
                    onChange={this.updateEggAllergen}
                  />
                  <span>Eggs</span>
                </label>
                <br />

                <label>
                  <input
                    type="checkbox"
                    name="soyAllergen"
                    defaultChecked={this.state.soyAllergen}
                    onChange={this.updateSoyAllergen}
                  />
                  <span>Soy</span>
                </label>
                <br />

                <label>
                  <input
                    type="checkbox"
                    name="fishAllergen"
                    defaultChecked={this.state.fishAllergen}
                    onChange={this.updateFishAllergen}
                  />
                  <span>Fish</span>
                </label>
                <br />

                <label>
                  <input
                    type="checkbox"
                    name="shellFishAllergen"
                    defaultChecked={this.state.shellFishAllergen}
                    onChange={this.updateShellFishAllergen}
                  />
                  <span>Shellfish</span>
                </label>
                <br />

                <label>
                  <input
                    type="checkbox"
                    name="eggAllergen"
                    defaultChecked={this.state.wheatAllergen}
                    onChange={this.updateWheatAllergen}
                  />
                  <span>Wheat</span>
                </label>
                <br />

                <label>
                  <input
                    type="checkbox"
                    name="treeNutsAllergen"
                    defaultChecked={this.state.treeNutsAllergen}
                    onChange={this.updateTreeNutsAllergen}
                  />
                  <span>Treenuts</span>
                </label>
                <br />

                <label>
                  <input
                    type="checkbox"
                    name="peanutsAllergen"
                    defaultChecked={this.state.peanutsAllergen}
                    onChange={this.updatePeanutsAllergen}
                  />
                  <span>Peanuts</span>
                </label>
              </div>
            </div>
          </div>

          <div className="row">
            <Input
              s={4}
              type='select'
              label="Active"
              id="active"
              className="active"
              name="active"
              value={this.state.active}
              onChange={evt => this.updateActive(evt)}
            >
              <option value='0'>Non-Active</option>
              <option value='1'>Active</option>
            </Input>
          </div>

          <div className="row">
            <Input
              s={4}
              type='select'
              label="Featured"
              id="featured"
              className="featured"
              name="featured"
              value={this.state.featured}
              onChange={evt => this.updateFeatured(evt)}
            >
              <option value='false'>Not Featured</option>
              <option value='true'>Featured</option>
            </Input>
          </div>

          <div className="row">
            <div className="input-field col s10">
              <input
                id="featured-headline"
                type="text"
                className="featured-headline"
                name="featuredHeadline"
                value={this.state.featuredHeadline}
                onChange={evt => this.updateFeaturedHeadline(evt)}
              />
              <label htmlFor="featured-headline" className={this.state.context === 'Edit' && 'active'}>Featured Headline</label>
            </div>
          </div>

          <div className="row">
            <Input
              s={4}
              type='select'
              label="Published"
              id="published"
              className="published"
              name="published"
              value={this.state.published}
              onChange={evt => this.updatePublished(evt)}
            >
              <option value='0'>Unpublished</option>
              <option value='1'>Published</option>
            </Input>
          </div>


          <button className="btn confirm modal-action modal-close right">
            {this.state.context === 'Edit' ? <span>Update Ingredient</span> : <span>Add Ingredient</span>}
          </button>

          <br />
          <br />
          <br />

          {this.state.context === 'Edit' &&
            <button
              className="btn delete modal-action modal-close right"
              type="button"
              onClick={this.handleDelete}
            >
              Delete
            </button>
          }
        </form>
      </div>
    )
  }
}

export default AddIngredient
