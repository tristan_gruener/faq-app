import React from 'react'
import { Input } from 'react-materialize'
import _ from 'lodash'
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import '../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';


class AddNordicStandard extends React.Component {
  constructor(props){
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleDelete = this.handleDelete.bind(this)

    console.log(this.props.location.state)



    if(_.isEmpty(this.props.location.state)){
      console.log('add mode')

      const html = '<p>Hey this <strong>editor</strong> rocks 😀</p>'
      const contentBlock = htmlToDraft(html)
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks)
        const editorState = EditorState.createWithContent(contentState)
        this.state = {
          context: 'Add',
          id: 0,
          name: '',
          description: '',
          featured: false,
          featuredHeadline: '',
          // standard: '',
          editorState
        }
      }
    } else {
      console.log('edit mode')

      const html = this.props.location.state.standard
      const contentBlock = htmlToDraft(html)
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks)
        const editorState = EditorState.createWithContent(contentState)


        this.state = {
          context: 'Edit',
          id: this.props.location.state.id,
          name: this.props.location.state.name,
          description: this.props.location.state.description,
          featured: this.props.location.state.featured,
          featuredHeadline: this.props.location.state.featuredHeadline,
          // standard: this.props.location.state.standard,
          // const editorState = EditorState.createWithContent(contentState);
          editorState,
          published: this.props.location.state.published === true ? 1 : 0
        }
      }
    }
  }

  updateName(evt){
    this.setState({
      name: evt.target.value
    })
  }

  updateDescription(evt){
    this.setState({
      description: evt.target.value
    })
  }

  // updateStandard(evt){
  //   this.setState({
  //     standard: evt.target.value
  //   })
  // }

  updatePublished(evt){
    this.setState({
      published: evt.target.value
    });
  }

  handleSubmit(e) {
    e.preventDefault()

    const id = this.state.id
    const name = this.state.name
    const description = this.state.description
    // const standard = this.state.standard
    const standard = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))
    const published = this.state.published
    const featured = this.state.featured
    const featuredHeadline = this.state.featuredHeadline

    let method = ''
    let url = ''
    // const url = '/faq/standard/'


    const data = {
      'id': id,
      'name': name,
      'description': description,
      'standard': standard,
      'published': published,
      'featured': featured,
      'featured_headline': featuredHeadline
    }

    switch (this.state.context) {
      case 'Add':
        method = 'POST'
        url = `/faq/standard/`
        break;

      case 'Edit':
        method = 'PUT'
        url = `/faq/standard/${this.state.id}/`
        break;
    }

    this.pushToDb(url, method, data)
  }

  pushToDb(url, method, data){
    fetch(url, {
      method: method,
      body: JSON.stringify(data),
      headers: {
        'Authorization': ' Token ' + localStorage.getItem('token'),
        'Content-Type': 'application/json'
      }
    }).then(res => res.json()).then(response => {
      console.log(response)
      // if(method === 'POST') {
      //    alert('succesfully added item')
      // } else {
      //   alert('succesfully edited item')
      // }

      const url = `/faqs/nordic-standard/${response.id}/`
      this.props.history.push(url)
      window.scrollTo(0,0)

    })
  }

  handleDelete(){
    const url = `/faq/standard/${this.state.id}/`
    const deleteConfirmation = window.confirm("Are you sure you want to delete this Nordic Standard?");
    if (deleteConfirmation) {
      fetch(url, {
        method: 'DELETE',
        headers: {
          'Authorization': ' Token ' + localStorage.getItem('token'),
          'Content-Type': 'application/json'
        }
      }).then(() => {
        window.location.assign('/faqs/');
      })
    }
  }

  onEditorStateChange: Function = (editorState) => {
    this.setState({
      editorState,
    })
  }

  updateFeatured(evt){
    this.setState({
      featured: evt.target.value
    });
  }

  updateFeaturedHeadline(evt){
    this.setState({
      featuredHeadline: evt.target.value
    });
  }

  render() {
    return (
      <div>
        <h2>{this.state.context} Nordic Standard</h2>

        <form>
          <div className="row">
            <div className="input-field col s10">
              <input
                id="name"
                type="text"
                className="validate"
                name="name"
                value={this.state.name}
                onChange={evt => this.updateName(evt)}
              />
              <label htmlFor="name" className={this.state.context === 'Edit' && 'active'}>Name</label>
            </div>
          </div>

          <div className="row">
            <div className="input-field col s10">
              <input
                id="description"
                type="text"
                className="validate"
                name="description"
                value={this.state.description}
                onChange={evt => this.updateDescription(evt)}
              />
              <label htmlFor="description" className={this.state.context === 'Edit' && 'active'}>Description</label>
            </div>
          </div>





          {/* WYSIWYG goes to the Standard Field */}




          {/* <div className="row">
            <div className="input-field col s10">
              <input
                id="standard"
                type="text"
                className="validate"
                name="standard"
                value={this.state.standard}
                onChange={evt => this.updateStandard(evt)}
              />
              <label htmlFor="standard" className={this.state.context === 'Edit' && 'active'}>Standard</label>
            </div>
          </div> */}

          <div className="row">
            <div className="col s10">
              <Editor
                editorState={this.state.editorState}
                onEditorStateChange={this.onEditorStateChange}
                wrapperClassName="wrapper-class"
                editorClassName="editor-class"
                toolbarClassName="toolbar-class"
                toolbar={{
                  inline: { inDropdown: true },
                  list: { inDropdown: true },
                  textAlign: { inDropdown: true },
                  link: { inDropdown: true },
                  history: { inDropdown: true },
                  font: { inDropdown: false }
                }}
                editorStyle={{
                  "border": "1px solid #F1F1F1",
                  "height": "320px",
                  "maxHeight": "100%",
                  "marginBottom": "20px",
                  "paddingLeft": "18px",
                  "paddingRight": "18px"
                }}
              />
            </div>
          </div>


          <div className="row">
            <Input
              s={4}
              type='select'
              label="Featured"
              id="featured"
              className="featured"
              name="featured"
              value={this.state.featured}
              onChange={evt => this.updateFeatured(evt)}
            >
              <option value='false'>Not Featured</option>
              <option value='true'>Featured</option>
            </Input>
          </div>

          <div className="row">
            <div className="input-field col s10">
              <input
                id="featured-headline"
                type="text"
                className="featured-headline"
                name="featuredHeadline"
                value={this.state.featuredHeadline}
                onChange={evt => this.updateFeaturedHeadline(evt)}
              />
              <label htmlFor="featured-headline" className={this.state.context === 'Edit' && 'active'}>Featured Headline</label>
            </div>
          </div>




          <div className="row">
            <Input
              s={4}
              type='select'
              label="Published"
              id="published"
              className="published"
              name="published"
              value={this.state.published}
              onChange={evt => this.updatePublished(evt)}
            >
              <option value='0'>Unpublished</option>
              <option value='1'>Published</option>
            </Input>
          </div>

          <button className="btn confirm right" onClick={this.handleSubmit}>
            <span>{this.state.context} Standard</span>
          </button>
          <br />
          <br />
          <br />
          {this.state.context === 'Edit' &&
            <button
              className="btn delete modal-action modal-close right"
              type="button"
              onClick={this.handleDelete}
            >
              Delete
            </button>
          }

        </form>
      </div>
    )
  }
}

export default AddNordicStandard
