import React from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
import _ from 'lodash'
import radix from 'radix'
// import Search from './Search'

import ProductLinks from './ProductLinks'

class SideNav extends React.Component {
  constructor(){
    super()
    this.state = {
      nordicStandards: [],
      productCount: 0,
      ingredients: [],
      actives: [],
      nonActives: [],
      uncategorizedIngredients: [],
      omega3s: [],
      probiotics: [],
      vitaminsAndMore: [],
      gummiesProd: [],
      uncategorizedProds: [],
      bulks: [],
      softGelBulk: [],
      gummiesBulk: [],
      liquidBulk: [],
      capsuleBulk: [],
      otherBulk: [],
      uncategorizedBulks: [],
      unpublishedProducts: [],
      searchInput: '',
      showResults: false,
      dataSet: [
        {
          'name': 'First',
          'id': 1
        },
        {
          'name': 'Second',
          'id': 2
        },
        {
          'name': 'Third',
          'id': 3
        }
      ],
      searchResults: []
    }
  }

  componentDidMount(){
    this.getIngredients()
    this.getProducts()
    this.getBulks()
    this.getNordicStandards()
  }

  getIngredients(){
    // axios.get('https://localhost:4430/faq/ingredient/')
    axios.get('/faq/ingredient/')
      .then(res => {
        const publishedIngredients = []
        for (var i = 0; i < res.data.length; i++) {
          if (res.data[i].published === true){
            publishedIngredients.push(res.data[i])
          }
        }

        const sortedIngredients = _.orderBy(publishedIngredients, [ingredient => ingredient.description.toLowerCase().trim().split('-')[2]], ['asc']);

        const actives = []
        const nonActives = []
        const uncategorized = []
        for (const ingredient in sortedIngredients){
          if(sortedIngredients[ingredient].active === true){
            actives.push(sortedIngredients[ingredient])
          }else if(sortedIngredients[ingredient].active === false){
            nonActives.push(sortedIngredients[ingredient])
          }else{
            uncategorized.push(sortedIngredients[ingredient])
          }
        }
        this.setState({
          actives,
          nonActives,
          uncategorizedIngredients: uncategorized,
          ingredients: sortedIngredients
        })

        res.data.map(rm => {
          localStorage.setItem(`${rm.id}`, `${rm.description}`)
        })
      })
  }

  getProducts(){
    // axios.get('https://localhost:4430/faq/product/')
    axios.get('/faq/product/')
      .then(res => {
        const publishedProducts = []
        for (var i = 0; i < res.data.length; i++) {
          if (res.data[i].published === true){
            publishedProducts.push(res.data[i])
          }
        }

        //do variant operations BEFORE sorting the products
        //what are we trying to do here? get the localStorage all loaded up correctly on the app init

        //map over the published products to determine which of the
        //published products have variants with them
        publishedProducts.map(product => {
          //run a get so you can get a full peep at the data
          // axios.get(`https://localhost:4430/faq/product/${product.id}/`).then(res => {
          axios.get(`/faq/product/${product.id}/`).then(res => {
            localStorage.setItem(`product-${product.id}`, res.data.bulk)

            const variants = JSON.parse(res.data.info_pages[0].html_j).variations
            if(JSON.parse(variants.length > 1)){
              localStorage.setItem(`product-with-variant-${product.id}`, JSON.stringify(variants))
            }
          })
        })

        const sortedProducts = _.orderBy(publishedProducts, [product => product.published], ['asc']);

        //get omega3s... 4
        const omega3s = []
        //get probiotics... 5
        const probiotics = []
        //get vitaminsAndMore... 6
        const vitaminsAndMore = []
        //get gummies... 7
        const gummiesProd = []
        //default
        const uncategorized = []

        for(const i in sortedProducts){
          const typeId = parseInt(sortedProducts[i].product_type.split('/')[5])
          const prod = sortedProducts[i]
          switch(typeId){
            case 4: {
              omega3s.push(prod)
              break;
            }
            case 5: {
              probiotics.push(prod)
              break;
            }
            case 6: {
              vitaminsAndMore.push(prod)
              break;
            }
            case 7: {
              gummiesProd.push(prod)
              break;
            }
            default: {
              uncategorized.push(prod)
            }
          }
        }

        const dataSet = this.state.dataSet
        dataSet.concat(sortedProducts)

        //what do i want to save now in the localStorage
        //product and their bulks
        //product and its bulk
        //the id of the product and the id of its bulk

        //if i captcher this i can run the two against each other and eliminate all the api calls
        //just stick with localStorage here

        //what does running the two agains each other mean?
        //i have a list where you enter the bulk id and you get that bulk ids ingredients
        // --> localStorage('bulk-888', 'ingredientsInBulk')
        //this will be a list where you enter the product id and you get that products bulk
        // --> localStorage('product-444', 'bulkInProduct')

        //so when you are on the ingredient page you can loop through all the products
          //then with each product id you can enter it into the first vending machine
          // --> localStorage('product-444', 'bulkInProduct')
          //then you enter that result (the bulk) into the next vending machine
          // --> localStorage('bulk-888', 'ingredientsInBulk')

          //with the ingredientsInBulk you will be able to loop through them and determine if its the same as the ingredient you are currently on
          //because if it is that means the product id you entered into the vending machine should be in the list



        this.setState({
          productCount: sortedProducts.length,
          omega3s,
          probiotics,
          vitaminsAndMore,
          gummiesProd,
          dataSet
        })


      })
  }

  getBulks(){
    // axios.get('https://localhost:4430/faq/bulk/')
    axios.get('/faq/bulk/')
      .then(res => {
        console.log('bulk test')
        console.log(res.data)
        const publishedBulks = []
        for (var i = 0; i < res.data.length; i++) {
          if (res.data[i].published === true){
            publishedBulks.push(res.data[i])
          }
        }

        const sortedBulks = _.orderBy(publishedBulks, [bulk => bulk.published], ['asc']);

        //get soft gels... 1
        const softGelBulk = []
        //get gummies... 2
        const gummiesBulk = []
        //get vitaminsAndMore... 3
        const liquidBulk = []
        const capsuleBulk = []
        const otherBulk = []
        //default
        const uncategorizedBulks = []

        for(const i in sortedBulks){
          let typeId = null
          if(sortedBulks[i].bulk_type !== null) {
            typeId = parseInt(sortedBulks[i].bulk_type.split('/')[5])
            switch(typeId){
              case 1: {
                softGelBulk.push(sortedBulks[i])
                break;
              }
              case 2: {
                gummiesBulk.push(sortedBulks[i])
                break;
              }
              case 3: {
                liquidBulk.push(sortedBulks[i])
                break;
              }
              case 12: {
                capsuleBulk.push(sortedBulks[i])
                break;
              }
              case 13: {
                otherBulk.push(sortedBulks[i])
                break;
              }
            }
          } else {
            uncategorizedBulks.push(sortedBulks[i])
          }
        }

        this.setState({
          bulks: sortedBulks,
          softGelBulk,
          gummiesBulk,
          liquidBulk,
          capsuleBulk,
          otherBulk,
          uncategorizedBulks: uncategorizedBulks
        })


        sortedBulks.map(bulky => {
          // axios.get(`https://localhost:4430/faq/bulk/${bulky.id}/`).then(res => {
          axios.get(`/faq/bulk/${bulky.id}/`).then(res => {
            localStorage.setItem(`bulkWithIngredient-${bulky.id}`, JSON.stringify(res.data.ingredients))
          })
        })

        res.data.map(bulk => {
          localStorage.setItem(`bulk-${bulk.id}`, `${bulk.name}`)
        })
      })
  }

  getNordicStandards() {

    //why am i not seeing published standards in the list?
    //lets do some local testing

    // axios.get('https://localhost:4430/faq/standard/').then(res => {
    axios.get('/faq/standard/').then(res => {
      //we got an array of objects
      // const sortedNordicStandards = _.orderBy(res.data, [ns => ns.name], ['asc']);
      console.log(res.data)

      const publishedNordicStandards = []
      for (var i = 0; i < res.data.length; i++) {
        if (res.data[i].published === true){
          publishedNordicStandards.push(res.data[i])
        }
      }

      console.log(publishedNordicStandards)

      //so weird for some reason i can't hit the published field when i query... i need to search each one individually say it aint so

      const sortedNordicStandards = _.orderBy(publishedNordicStandards, [ns => ns.name], ['asc']);

      this.setState({
        nordicStandards: sortedNordicStandards
      })
    })
  }

  updateSearchInput(evt){
    const searchText = evt.target.value.toLowerCase()
    const searchResults = []
    this.state.dataSet.map(item => {
      if(item.description.toLowerCase().indexOf(searchText) > -1) {
        searchResults.push(item)
      }
    })

    if(evt.target.value.length > 0){
      this.setState({
        showResults: true,
        searchInput: evt.target.value,
        searchResults: searchResults
      })
    } else {
      this.setState({
        showResults: false,
        searchInput: evt.target.value,
        searchResults: searchResults
      })
    }
  }

  render(){
    return(
      <div id="side-nav">
        <ul>
          {this.state.unpublishedProducts.map(bulk => {
            let url = "/bulk/" + bulk.id
            return <li className="sub-item" key={bulk.id}><Link to={url}>{bulk.name}</Link></li>
          })}
        </ul>
        {/* <Search /> */}
        <ul className="collapsible expandable">
          <li>
  {/* PRODUCTS */}
            <div className="collapsible-header">
              <i className="fas fa-prescription-bottle"></i><b>Products</b> <span className="badge">
                {this.state.productCount === 0 ? <i className="fas fa-spinner clockwise"></i> : this.state.productCount}
              </span>
            </div>
            <div className="collapsible-body">
              <ul className="collapsible" id="inner-dropdown">
                <ProductLinks
                  productGroupName='Omega-3s'
                  length={this.state.omega3s.length}
                  prodGroup={this.state.omega3s}
                />
                <ProductLinks
                  productGroupName='Probiotics & More'
                  length={this.state.probiotics.length}
                  prodGroup={this.state.probiotics}
                />
                <ProductLinks
                  productGroupName='Vitamins & More'
                  length={this.state.vitaminsAndMore.length}
                  prodGroup={this.state.vitaminsAndMore}
                />
                <ProductLinks
                  productGroupName='Gummies'
                  length={this.state.gummiesProd.length}
                  prodGroup={this.state.gummiesProd}
                />
                <ProductLinks
                  productGroupName='Uncategorized'
                  length={this.state.uncategorizedProds.length}
                  prodGroup={this.state.uncategorizedProds}
                />
              </ul>
            </div>
          </li>
  {/* END PRODUCTS */}
  {/* INGREDIENTS */}
          <li>
            <div className="collapsible-header">
              <i className="fas fa-flask"></i>
              <b>Raw Materials</b> <span className="badge">{this.state.ingredients.length === 0 ? <i className="fas fa-spinner clockwise"></i> : this.state.ingredients.length}</span>
            </div>
            <div className="collapsible-body">
              <ul className="collapsible" id="inner-dropdown-ingredients">
                <li>
                  <div className="collapsible-header">
                    Actives <span className="badge">{this.state.actives.length}</span>
                  </div>
                  <div className="collapsible-body">
                    <ul className="nested-ul">
                      {this.state.actives.map(active => {
                        let url = "/faqs/ingredient/" + active.id
                        return <li className="sub-item" key={active.id}><Link to={url}>{active.description}</Link></li>
                      })}
                    </ul>
                  </div>
                </li>
                <li>
                  <div className="collapsible-header">
                    Non-Actives <span className="badge">{this.state.nonActives.length}</span>
                  </div>
                  <div className="collapsible-body">
                    <ul className="nested-ul">
                      {this.state.nonActives.map(nonActive => {
                        let url = "/faqs/ingredient/" + nonActive.id
                        return <li className="sub-item" key={nonActive.id}><Link to={url}>{nonActive.description}</Link></li>
                      })}
                    </ul>
                  </div>
                </li>
                <li>
                  <div className="collapsible-header">
                    Uncategorized <span className="badge">{this.state.uncategorizedIngredients.length}</span>
                  </div>
                  <div className="collapsible-body">
                    <ul className="nested-ul">
                      {this.state.uncategorizedIngredients.map(ingredient => {
                        let url = "/faqs/ingredient/" + ingredient.id
                        return <li className="sub-item" key={ingredient.id}><Link to={url}>{_.startCase(_.toLower(ingredient.description))}</Link></li>
                      })}
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </li>
  {/* END INGREDIENTS */}
  {/* BULKS */}
          <li>
            <div className="collapsible-header">
              <i className="fas fa-mortar-pestle"></i>
              {/* <b>Bulks</b> <span className="badge">{this.state.bulks.length}</span> */}
              <b>Bulks</b> <span className="badge">{this.state.bulks.length === 0 ? <i className="fas fa-spinner clockwise"></i> : this.state.bulks.length}</span>
            </div>
            <div className="collapsible-body">
              <ul className="collapsible" id="inner-dropdown-ingredients">
                <li>
                  <div className="collapsible-header">
                    Soft Gel <span className="badge">{this.state.softGelBulk.length}</span>
                  </div>
                  <div className="collapsible-body">
                    <ul className="nested-ul">
                      {this.state.softGelBulk.map(bulk => {
                        let url = "/faqs/bulk/" + bulk.id
                        return (
                          <li className="sub-item" key={bulk.id}>
                            <Link to={{

                              pathname: `/faqs/bulk/${bulk.id}`,
                              state: { bulkId: bulk.id }
                            }}>
                              {bulk.name}
                            </Link>
                          </li>
                        )
                      })}
                    </ul>
                  </div>
                </li>
                <li>
                  <div className="collapsible-header">
                    Capsule <span className="badge">{this.state.capsuleBulk.length}</span>
                  </div>
                  <div className="collapsible-body">
                    <ul className="nested-ul">
                      {this.state.capsuleBulk.map(bulk => {
                        let url = "/faqs/bulk/" + bulk.id
                        return (
                          <li className="sub-item" key={bulk.id}>
                            <Link to={{

                              pathname: `/faqs/bulk/${bulk.id}`,
                              state: { bulkId: bulk.id }
                            }}>
                              {bulk.name}
                            </Link>
                          </li>
                        )
                      })}
                    </ul>
                  </div>
                </li>
                <li>
                  <div className="collapsible-header">
                    Gummies <span className="badge">{this.state.gummiesBulk.length}</span>
                  </div>
                  <div className="collapsible-body">
                    <ul className="nested-ul">
                      {this.state.gummiesBulk.map(bulk => {
                        let url = "/faqs/bulk/" + bulk.id
                        return (
                          <li className="sub-item" key={bulk.id}>
                            <Link to={{

                              pathname: `/faqs/bulk/${bulk.id}`,
                              state: { bulkId: bulk.id }
                            }}>
                              {bulk.name}
                            </Link>
                          </li>
                        )
                      })}
                    </ul>
                  </div>
                </li>
                <li>
                  <div className="collapsible-header">
                    Liquid <span className="badge">{this.state.liquidBulk.length}</span>
                  </div>
                  <div className="collapsible-body">
                    <ul className="nested-ul">
                      {this.state.liquidBulk.map(bulk => {
                        let url = "/faqs/bulk/" + bulk.id
                        return (
                          <li className="sub-item" key={bulk.id}>
                            <Link to={{

                              pathname: `/faqs/bulk/${bulk.id}`,
                              state: { bulkId: bulk.id }
                            }}>
                              {bulk.name}
                            </Link>
                          </li>
                        )
                      })}
                    </ul>
                  </div>
                </li>
                <li>
                  <div className="collapsible-header">
                    Other <span className="badge">{this.state.otherBulk.length}</span>
                  </div>
                  <div className="collapsible-body">
                    <ul className="nested-ul">
                      {this.state.otherBulk.map(bulk => {
                        let url = "/faqs/bulk/" + bulk.id
                        return (
                          <li className="sub-item" key={bulk.id}>
                            <Link to={{

                              pathname: `/faqs/bulk/${bulk.id}`,
                              state: { bulkId: bulk.id }
                            }}>
                              {bulk.name}
                            </Link>
                          </li>
                        )
                      })}
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </li>
  {/* END BULKS */}






  {/* NORDIC STANDARDS

    i need to sort these! first so they are in the correct order


    */}

          <li>

            <div className="collapsible-header">
              {/* <i className="fas fa-mortar-pestle"></i> */}
              <i className="fas fa-clipboard"></i>
              {/* <b>Bulks</b> <span className="badge">{this.state.bulks.length}</span> */}
              <b>Nordic Standards</b> <span className="badge">{this.state.nordicStandards.length === 0 ? <i className="fas fa-spinner clockwise"></i> : this.state.nordicStandards.length}</span>
            </div>

            <div className="collapsible-body">
              <ul className="collapsible" id="inner-dropdown-ingredients">
                <li>

                    <ul className="nested-ul">
                      {this.state.nordicStandards.map(standard => {
                        let url = "/faqs/standard/" + standard.id
                        return (
                          <li className="sub-item" key={standard.id}>
                            <Link to={{

                              pathname: `/faqs/nordic-standard/${standard.id}`,
                              state: { nordicStandardId: standard.id }
                            }}>
                              {standard.name}
                            </Link>
                          </li>
                        )
                      })}
                    </ul>
                  
                </li>



                {/* <li>
                  <div className="collapsible-header">
                    Other <span className="badge">{this.state.otherBulk.length}</span>
                  </div>
                  <div className="collapsible-body">
                    <ul className="nested-ul">
                      {this.state.otherBulk.map(bulk => {
                        let url = "/faqs/bulk/" + bulk.id
                        return (
                          <li className="sub-item" key={bulk.id}>
                            <Link to={{

                              pathname: `/faqs/bulk/${bulk.id}`,
                              state: { bulkId: bulk.id }
                            }}>
                              {bulk.name}
                            </Link>
                          </li>
                        )
                      })}
                    </ul>
                  </div>
                </li> */}
              </ul>

            </div>

          </li>
  {/* END BULKS */}






        </ul>
      </div>
    )
  }
}


export default SideNav
