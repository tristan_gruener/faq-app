import React from 'react'
import { Link } from 'react-router-dom'

class ProductLinks extends React.Component {

  prodUrl(prod){
    return "/product/" + prod.split(' ').join('-').toLowerCase()
  }

  render(){
    return(
      <li>
        <div className="collapsible-header">
          {this.props.productGroupName} <span className="badge">{this.props.length}</span>
        </div>
        <div className="collapsible-body">
          <ul className="nested-ul">
            {this.props.prodGroup.map(product => (
              <li className="sub-item" key={product.id}>
                <Link to={{
                  pathname: `/faqs/product/${product.id}`,
                  state: { prodId: product.id }
                }}>
                  {product.description}
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </li>
    )
  }
}

export default ProductLinks
