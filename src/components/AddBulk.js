import React from 'react'
import { Input } from 'react-materialize'
import _ from 'lodash'

class AddBulk extends React.Component {
  constructor(props){
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.setBulkType = this.setBulkType.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.updateManufactureWarning = this.updateManufactureWarning.bind(this)
    this.updateMilkAllergen = this.updateMilkAllergen.bind(this)
    this.updateEggAllergen = this.updateEggAllergen.bind(this)
    this.updateSoyAllergen = this.updateSoyAllergen.bind(this)
    this.updateFishAllergen = this.updateFishAllergen.bind(this)
    this.updateShellFishAllergen = this.updateShellFishAllergen.bind(this)
    this.updateWheatAllergen = this.updateWheatAllergen.bind(this)
    this.updateTreeNutsAllergen = this.updateTreeNutsAllergen.bind(this)
    this.updatePeanutsAllergen = this.updatePeanutsAllergen.bind(this)
    this.getRawMaterials = this.getRawMaterials.bind(this)
    this.updateRawMaterial = this.updateRawMaterial.bind(this)
    this.removeRawMaterialInBulk = this.removeRawMaterialInBulk.bind(this)

    // ----
    // ADD
    // ----
    if(_.isEmpty(this.props.location.state)){
      this.state = {
        context: 'Add',
        bulkName: '',
        bulkType: 'Uncategorized',
        bulkImage: '',
        shelfLife: '',
        manufactureLocation: '',
        manufactureWarning: false,
        certifications: '',
        milkAllergen: false,
        eggAllergen: false,
        soyAllergen: false,
        fishAllergen: false,
        shellFishAllergen: false,
        wheatAllergen: false,
        treeNutsAllergen: false,
        peanutsAllergen: false,
        rawMaterial: '',
        rawMaterials: ['a', 'b'],
        rawMaterialsInBulk: [],
        rawMaterialsInBulkRender: [],
        featured: false,
        featuredHeadline: ''
      }
    }else{
      // ----
      // EDIT
      // ----
      this.state = {
        context: this.props.location.state.context,
        bulkId: this.props.location.state.id,
        infoPageId: this.props.location.state.infoPageId,
        needsInfoPageOnEdit: this.props.location.state.needsInfoPageOnEdit,
        bulkName: this.props.location.state.name,
        bulkType: this.setBulkType(this.props.location.state.bulkType),
        bulkImage: this.props.location.state.bulkImage,
        shelfLife: this.props.location.state.shelfLife,
        manufactureLocation: this.props.location.state.manufactureLocation,
        manufactureWarning: this.props.location.state.manufactureWarning,
        certifications: this.props.location.state.certifications,
        milkAllergen: this.props.location.state.milkAllergen,
        eggAllergen: this.props.location.state.eggAllergen,
        soyAllergen: this.props.location.state.soyAllergen,
        fishAllergen: this.props.location.state.fishAllergen,
        shellFishAllergen: this.props.location.state.shellFishAllergen,
        wheatAllergen: this.props.location.state.wheatAllergen,
        treeNutsAllergen: this.props.location.state.treeNutsAllergen,
        peanutsAllergen: this.props.location.state.peanutsAllergen,
        published: this.props.location.state.published === true ? 1 : 0,
        rawMaterialsInBulk: this.props.location.state.rawMaterialsInBulk,
        rawMaterialsInBulkRender: this.props.location.state.rawMaterialsInBulkRender,
        featured: this.props.location.state.featured,
        featuredHeadline: this.props.location.state.featuredHeadline
      }
    }
    this.getRawMaterials();
  }

  setBulkType(bulkTypeUrl){
    const typeId = parseInt(bulkTypeUrl.split('/')[5])
    let bulkTypeRef = 'null'
    switch(typeId){
      case 1:
        bulkTypeRef = '/faq/product-type/1/'
        break
      case 2:
        bulkTypeRef = '/faq/product-type/2/'
        break
      case 3:
        bulkTypeRef = '/faq/product-type/3/'
        break
      case 12:
        bulkTypeRef = '/faq/product-type/12/'
        break
      case 13:
        bulkTypeRef = '/faq/product-type/13/'
        break
      default:
        bulkTypeRef = 'null'
    }
    return bulkTypeRef
  }

  handleSubmit(e){
    e.preventDefault()
    const bulkName = e.target.elements.bulkName.value
    const published = e.target.elements.published.value
    const bulkType = e.target.elements.bulkType.value
    const shelfLife = e.target.elements.shelfLife.value
    const bulkImage = e.target.elements.bulkImage.value
    const manufactureLocation = e.target.elements.manufactureLocation.value
    const manufactureWarning = e.target.elements.manufactureWarning.value
    const certifications = e.target.elements.certifications.value
    const milkAllergen = this.state.milkAllergen
    const eggAllergen = this.state.eggAllergen
    const soyAllergen = this.state.soyAllergen
    const fishAllergen = this.state.fishAllergen
    const shellFishAllergen = this.state.shellFishAllergen
    const wheatAllergen = this.state.wheatAllergen
    const treeNutsAllergen = this.state.treeNutsAllergen
    const peanutsAllergen = this.state.peanutsAllergen
    const featured = e.target.elements.featured.value
    const featuredHeadline = e.target.elements.featuredHeadline.value

    console.log('on submit')
    console.log(featuredHeadline)

    let url = '/faq/bulk/'
    let method = ''
    let data = {}
    const infoPageData = {
      manufacturing: {
        'shelfLife': shelfLife,
        'manufactureLocation': manufactureLocation,
        'certifications': certifications
      },
      allergens: {
        'manufactureWarning': manufactureWarning,
        'milkAllergen': milkAllergen,
        'eggAllergen': eggAllergen,
        'soyAllergen': soyAllergen,
        'fishAllergen': fishAllergen,
        'shellFishAllergen': shellFishAllergen,
        'wheatAllergen': wheatAllergen,
        'treeNutsAllergen': treeNutsAllergen,
        'peanutsAllergen': peanutsAllergen
      }
    }

    switch (this.state.context) {
      case 'Add':
        method = 'POST'
        // ----
        // NEW BULK PAGE
        // ----
        data = {
          'name': bulkName,
          'published': published,
          'bulk_type': bulkType,
          'ingredients': this.state.rawMaterialsInBulk,
          'image_urls': [
            bulkImage
          ],
          'faqs': [],
          'info_pages': [{
            'html_j': JSON.stringify(infoPageData)
          }],
          'featured': featured,
          'featured_headline': featuredHeadline

        }
        this.pushToDb(url, method, data, 'add')
        break;

      case 'Edit':
        method = 'PUT'
        if (this.state.needsInfoPageOnEdit) {
          // ----
          // EDIT xTuple BULK PAGE
          // ----
          data = {
            'name': bulkName,
            'published': published,
            'bulk_type': bulkType,
            'ingredients': this.state.rawMaterialsInBulk,
            'image_urls': [
              bulkImage
            ],
            'faqs': [],
            'info_pages': [{
              'html_j': JSON.stringify(infoPageData)
            }],
            'featured': featured,
            'featured_headline': featuredHeadline
          }
        } else {
          // ----
          // EDIT NATIVE BULK PAGE
          // ----
          data = {
            'name': bulkName,
            'published': published,
            'bulk_type': bulkType,
            'ingredients': this.state.rawMaterialsInBulk,
            'image_urls': [
              bulkImage
            ],
            'faqs': [],
            'info_pages': [],
            'featured': featured,
            'featured_headline': featuredHeadline
          }
        }
        url = `/faq/bulk/${this.state.bulkId}/`
        this.pushToDb(url, method, data, 'edit', 'step-1')
        // ----
        // EDIT NATIVE INFO PAGE
        // ----
        data = {
          'html_j': JSON.stringify(infoPageData)
        }
        url = `/faq/info-page/${this.state.infoPageId}/`
        this.pushToDb(url, method, data, 'edit', 'step-2')
        break;
    }
  }

  pushToDb(url, method, data, delay, step){
    fetch(url, {
      method: method,
      body: JSON.stringify(data),
      headers: {
        'Authorization': ' Token ' + localStorage.getItem('token'),
        'Content-Type': 'application/json'
      }
    }).then(res => res.json()).then(response => {

      console.log(response)

      localStorage.setItem(`bulk-${response.id}`, `${response.name}`)
      localStorage.setItem(`bulkWithIngredient-${response.id}`, JSON.stringify(response.ingredients))

      if(delay === 'add') {
        alert('succesfully added item')
        const url = `/faqs/bulk/${response.id}/`
        this.props.history.push(url)
        window.scrollTo(0,0)
      }

      if(delay === 'edit') {
        if(step === 'step-1'){
          alert('successfully edited item')
          const url = `/faqs/bulk/${response.id}/`
          this.props.history.push(url)
          window.scrollTo(0,0)
        }
      }
    })
  }

  handleDelete(){
    const url = `/faq/bulk/${this.state.bulkId}/`
    const deleteConfirmation = window.confirm("Are you sure you want to delete this Bulk? (all the faqs that have been added will also be deleted)");
    if (deleteConfirmation) {
      fetch(url, {
        method: 'DELETE',
        headers: {
          'Authorization': ' Token ' + localStorage.getItem('token'),
          'Content-Type': 'application/json'
        }
      }).then(() => {
        window.location.assign('/faqs/')
      })
    }
  }

  getRawMaterials(){
    fetch(`/faq/ingredient/`).then((res) => res.json()).then((json) => {
    // fetch(`https://localhost:4430/faq/ingredient/`).then((res) => res.json()).then((json) => {
      const sortedRawMaterials = _.orderBy(json, [rm => rm.description], ['asc']);
      this.setState({
        rawMaterials: sortedRawMaterials
      })

      sortedRawMaterials.map(rm => {
        localStorage.setItem(`${rm.id}`, `${rm.description}`)
      })
    })
  }

  updateBulkName(evt){
    this.setState({
      bulkName: evt.target.value
    });
  }

  updatePublished(evt){
    this.setState({
      published: evt.target.value
    });
  }

  updateBulkType(evt){
    this.setState({
      bulkType: evt.target.value
    })
  }

  updateBulkImage(evt){
    this.setState({
      bulkImage: evt.target.value
    });
  }

  updateShelfLife(evt){
    this.setState({
      shelfLife: evt.target.value
    })
  }

  updateManufactureLocation(evt){
    this.setState({
      manufactureLocation: evt.target.value
    })
  }

  updateCertifications(evt){
    this.setState({
      certifications: evt.target.value
    })
  }

  updateRawMaterial(evt){
    const temp = JSON.parse(evt.target.value)
    const rmsInBulk = this.state.rawMaterialsInBulk
    rmsInBulk.push(temp.url)
    const rmsInBulkRender = this.state.rawMaterialsInBulkRender
    rmsInBulkRender.push(temp.description)

    // console.log('rms in bulk')
    // console.log(rmsInBulk)
    //
    // console.log('rms in bulk render')
    // console.log(rmsInBulkRender)

    this.setState({
      rawMaterial: temp.url,
      rawMaterialsInBulk: rmsInBulk,
      rawMaterialsInBulkRender: rmsInBulkRender
    })
  }

  removeRawMaterialInBulk(rm){
    const rmsInBulkRender = this.state.rawMaterialsInBulkRender
    const rmsInBulk = this.state.rawMaterialsInBulk
    const pos = rmsInBulkRender.indexOf(rm);
    rmsInBulkRender.splice(pos, 1)
    rmsInBulk.splice(pos, 1)
    this.setState({
      rawMaterialsInBulk: rmsInBulk,
      rawMaterialsInBulkRender: rmsInBulkRender
    })
  }

  updateManufactureWarning(evt){
    this.setState({
      manufactureWarning: evt.target.value
    })
  }

  updateMilkAllergen(evt){
    this.setState({
      milkAllergen: !this.state.milkAllergen
    })
  }

  updateEggAllergen(evt){
    this.setState({
      eggAllergen: !this.state.eggAllergen
    })
  }

  updateSoyAllergen(evt){
    this.setState({
      soyAllergen: !this.state.soyAllergen
    })
  }

  updateFishAllergen(evt){
    this.setState({
      fishAllergen: !this.state.fishAllergen
    })
  }

  updateShellFishAllergen(evt){
    this.setState({
      shellFishAllergen: !this.state.shellFishAllergen
    })
  }

  updateWheatAllergen(evt){
    this.setState({
      wheatAllergen: !this.state.wheatAllergen
    })
  }

  updateTreeNutsAllergen(evt){
    this.setState({
      treeNutsAllergen: !this.state.treeNutsAllergen
    })
  }

  updatePeanutsAllergen(evt){
    this.setState({
      peanutsAllergen: !this.state.peanutsAllergen
    })
  }

  updateFeatured(evt){
    this.setState({
      featured: evt.target.value
    });
  }

  updateFeaturedHeadline(evt){
    this.setState({
      featuredHeadline: evt.target.value
    });
  }


  render(){
    return(
      <div>
        <h2>{this.state.context === 'Edit' ? `Editing ${this.state.bulkName}` : 'Add Bulk'}</h2>
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="input-field col s10">
              <input
                id="bulk-name"
                type="text"
                className="validate"
                name="bulkName"
                value={this.state.bulkName}
                onChange={evt => this.updateBulkName(evt)}
              />
              <label htmlFor="bulk-name" className={this.state.context === 'Edit' && 'active'}>Bulk Name</label>
            </div>
          </div>

          <div className="row">
            <Input
              s={4}
              type='select'
              label="Bulk Type"
              id="bulkType"
              className="bulkType"
              name="bulkType"
              value={this.state.bulkType}
              onChange={evt => this.updateBulkType(evt)}
            >
              <option value='/faq/product-type/1/'>Soft Gels</option>
              <option value='/faq/product-type/12/'>Capsules</option>
              <option value='/faq/product-type/2/'>Gummies</option>
              <option value='/faq/product-type/3/'>Liquid</option>
              <option value='/faq/product-type/13/'>Other</option>
            </Input>
          </div>

          <div className="row">
              <div className="input-field col s7">
                <input
                  id="bulk-image"
                  type="text"
                  className="validate"
                  style={{"fontSize":"12px"}}
                  name="bulkImage"
                  value={this.state.bulkImage}
                  onChange={evt => this.updateBulkImage(evt)}
                />
                <label htmlFor="bulk-image" className={this.state.context === 'Edit' && 'active'}>Bulk Image Url</label>
              </div>

              <div className="col s3">
                <img src={this.state.bulkImage} style={{"width": "300px"}}/>
              </div>
          </div>

          <h4>Manufacturing</h4>

          <div className="row">
            <div className="input-field col s5">
              <input
                id="shelf-life"
                type="text"
                className="validate"
                name="shelfLife"
                value={this.state.shelfLife}
                onChange={evt => this.updateShelfLife(evt)}
              />
              <label htmlFor="shelf-life" className={this.state.context === 'Edit' && 'active'}>Shelf Life</label>
            </div>
          </div>

          <div className="row">
            <div className="input-field col s5">
              <input
                id="manufacture-location"
                type="text"
                className="validate"
                name="manufactureLocation"
                value={this.state.manufactureLocation}
                onChange={evt => this.updateManufactureLocation(evt)}
              />
              <label htmlFor="manufacture-location" className={this.state.context === 'Edit' && 'active'}>Manufacture Location</label>
            </div>
          </div>

          <div className="row">
            <div className="input-field col s5">
              <input
                id="certifications"
                type="text"
                className="validate"
                name="certifications"
                value={this.state.certifications}
                onChange={evt => this.updateCertifications(evt)}
              />
              <label htmlFor="certifications" className={this.state.context === 'Edit' && 'active'}>Certifications</label>
            </div>
          </div>

          <h4>Raw Materials in Bulk</h4>

          <div className="row">
            <Input
              s={6}
              type='select'
              label="RawMaterial"
              id="rawMaterial"
              className="rawMaterial"
              name="rawMaterial"
              value={this.state.rawMaterial}
              onChange={evt => this.updateRawMaterial(evt)}
            >
              {this.state.rawMaterials && this.state.rawMaterials.map(rm => {
                const temp = JSON.stringify(rm)
                return <option value={temp}>{rm.description}</option>
              })}
            </Input>
            <div className="col s4">
              <ul>
                {this.state.rawMaterialsInBulkRender && this.state.rawMaterialsInBulkRender.map(rm => (
                  <li>
                    <b>{rm}</b>
                    <span className="remove-button" onClick={evt => this.removeRawMaterialInBulk(rm)}>X</span>
                  </li>
                ))}
              </ul>
            </div>
          </div>

          <h4>Allergens</h4>

          <div className="row">
            <div className="input-field">
              <div className="col s6">
                <p><b>Is this product manufactured in a facility that processes soy, dairy, wheat, rye, or barley?</b></p>
              </div>
              <div className="col s4" style={{"paddingTop": "14px"}}>
                <label style={{"marginRight": "14px"}}>
                  <input name="manufactureWarning" type="radio" value={1} onClick={this.updateManufactureWarning} defaultChecked={this.state.manufactureWarning === 1 && true}
                  />
                  <span>Yes</span>
                </label>
                <label style={{"marginRight": "14px"}}>
                  <input name="manufactureWarning" type="radio" value={0} onClick={this.updateManufactureWarning} defaultChecked={this.state.manufactureWarning === 0 && true} />
                  <span>No</span>
                </label>
                <label>
                  <input name="manufactureWarning" type="radio" value={2} onClick={this.updateManufactureWarning} defaultChecked={this.state.manufactureWarning === 2 && true}
                  />
                  <span>N/A</span>
                </label>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="input-field">
              <div className="col s8">
                <p><b>Specific Allergens</b></p>
                <label>
                  <input
                    type="checkbox"
                    name="milkAllergen"
                    defaultChecked={this.state.milkAllergen}
                    onChange={this.updateMilkAllergen}
                  />
                  <span>Milk</span>
                </label>
                <br />

                <label>
                  <input
                    type="checkbox"
                    name="eggAllergen"
                    defaultChecked={this.state.eggAllergen}
                    onChange={this.updateEggAllergen}
                  />
                  <span>Eggs</span>
                </label>
                <br />

                <label>
                  <input
                    type="checkbox"
                    name="soyAllergen"
                    defaultChecked={this.state.soyAllergen}
                    onChange={this.updateSoyAllergen}
                  />
                  <span>Soy</span>
                </label>
                <br />

                <label>
                  <input
                    type="checkbox"
                    name="fishAllergen"
                    defaultChecked={this.state.fishAllergen}
                    onChange={this.updateFishAllergen}
                  />
                  <span>Fish</span>
                </label>
                <br />

                <label>
                  <input
                    type="checkbox"
                    name="shellFishAllergen"
                    defaultChecked={this.state.shellFishAllergen}
                    onChange={this.updateShellFishAllergen}
                  />
                  <span>Shellfish</span>
                </label>
                <br />

                <label>
                  <input
                    type="checkbox"
                    name="eggAllergen"
                    defaultChecked={this.state.wheatAllergen}
                    onChange={this.updateWheatAllergen}
                  />
                  <span>Wheat</span>
                </label>
                <br />

                <label>
                  <input
                    type="checkbox"
                    name="treeNutsAllergen"
                    defaultChecked={this.state.treeNutsAllergen}
                    onChange={this.updateTreeNutsAllergen}
                  />
                  <span>Treenuts</span>
                </label>
                <br />

                <label>
                  <input
                    type="checkbox"
                    name="peanutsAllergen"
                    defaultChecked={this.state.peanutsAllergen}
                    onChange={this.updatePeanutsAllergen}
                  />
                  <span>Peanuts</span>
                </label>
              </div>
            </div>
          </div>

          <div className="row">
            <Input
              s={4}
              type='select'
              label="Featured"
              id="featured"
              className="featured"
              name="featured"
              value={this.state.featured}
              onChange={evt => this.updateFeatured(evt)}
            >
              <option value='false'>Not Featured</option>
              <option value='true'>Featured</option>
            </Input>
          </div>

          <div className="row">
            <div className="input-field col s10">
              <input
                id="featured-headline"
                type="text"
                className="featured-headline"
                name="featuredHeadline"
                value={this.state.featuredHeadline}
                onChange={evt => this.updateFeaturedHeadline(evt)}
              />
              <label htmlFor="featured-headline" className={this.state.context === 'Edit' && 'active'}>Featured Headline</label>
            </div>
          </div>

          <div className="row">
            <Input
              s={4}
              type='select'
              label="Published"
              id="published"
              className="published"
              name="published"
              value={this.state.published}
              onChange={evt => this.updatePublished(evt)}
            >
              <option value='0'>Unpublished</option>
              <option value='1'>Published</option>
            </Input>
          </div>

          <button className="btn confirm modal-action modal-close right">
            {this.state.context === 'Edit' ? <span>Update Bulk</span> : <span>Add Bulk</span>}
          </button>

          <br />
          <br />
          <br />

          {this.state.context === 'Edit' &&
            <button
              className="btn delete modal-action modal-close right"
              type="button"
              onClick={this.handleDelete}
            >
              Delete
            </button>
          }
        </form>
      </div>
    )
  }
}

export default AddBulk
