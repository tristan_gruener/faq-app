import React from 'react'
import { Modal } from 'react-materialize'
import FaqList from './FaqList'
import { Link } from 'react-router-dom'

class ProductPage extends React.Component {
  constructor(){
    super()
    this.setProduct = this.setProduct.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.state = {
      generalInformationFaqs: [],
      safetyAndDosageFaqs: [],
      selectedVariant: 'variation-0',
      previousVariant: '',
      variations: [{}]
    }
  }

  componentDidMount() {
    this.init()
  }

  componentDidUpdate(prevProps, prevState, snapshot){
    if(this.props.match.params.productId !== prevProps.match.params.productId){
      this.init()
    }
  }

  init(){
    const urlId = this.props.location.pathname.split('/')[3]
    // dev
    fetch(`https://localhost:4430/faq/product/${urlId}/`)
    // prod
    // fetch(`/faq/product/${urlId}/`)
      .then((res) => res.json())
      .then((json) => {
        const productInfo = JSON.stringify(json)
        console.log('look here first')
        console.log(productInfo)
        const infoPageId = JSON.parse(productInfo).info_pages[0].id
        const infoPages = JSON.parse(JSON.parse(productInfo).info_pages[0].html_j)
        const generalInformation = infoPages.generalInformation
        const revisionInformation = infoPages.revisionInformation
        const variations = infoPages.variations
        this.setProduct(JSON.parse(productInfo), generalInformation, revisionInformation, variations, infoPageId)
        this.setFaqs(JSON.parse(productInfo).faqs)
      }
    );
  }

  setProduct(productInformation, generalInformation, revisionInformation, variations, infoPageId){
    this.setState({
      id: productInformation.id,
      description: productInformation.description,
      productImage: generalInformation.productImage,
      sku: productInformation.sku,
      bulk: productInformation.bulk !== null && productInformation.bulk.split('/')[5],
      bulkName: productInformation.bulk !== null && localStorage.getItem(`bulk-${productInformation.bulk.split('/')[5]}`),
      productType: productInformation.product_type,
      currentRevisionLabelNumber: revisionInformation.currentRevisionLabelNumber,
      revisionNotes: revisionInformation.revisionNotes,
      infoPageId: infoPageId,
      storageConditions: generalInformation.storageConditions,
      casePackCount: generalInformation.casePackCount,
      casePackDimensions: generalInformation.casePackDimensions,
      casePackUpc: generalInformation.casePackUpc,
      unitDimensions: generalInformation.unitDimensions,
      upc: generalInformation.upc,
      wholesalePrice: generalInformation.wholesalePrice,
      retailPrice: generalInformation.retailPrice,
      webPrice: generalInformation.webPrice,
      published: productInformation.published,
      variations: variations,
      featured: productInformation.featured,
      featuredHeadline: productInformation.featured_headline
    }, () => {
      console.log('look here')
      console.log(this.state)
    })
  }

  setFaqs(faqs){
    this.setState({
      generalInformationFaqs: faqs.filter(faq => faq.page_section === 'generalInformation'),
      safetyAndDosageFaqs: faqs.filter(faq => faq.page_section === 'safetyAndDosage')
    })
  }

  getProductImage(){
    const productImageUrl = this.state.productImage
    const that = this
    this.getImage(productImageUrl).then(function(successUrl){
      that.setState({
        productImage: successUrl,
        imageLoaded: true
      })
    }).catch(function(errorUrl){
      that.setState({
        imageLoaded: false
      })
    })
  }

  getImage(url){
    return new Promise(function(resolve, reject){
      var img = new Image()
      img.onload = function(){
        resolve(url)
      }
      img.onerror = function(){
        reject(url)
      }
      img.src = url
    })
  }

  handleDelete(){
    const url = `/faq/product/${this.state.id}/`
    const method = 'DELETE'
    fetch(url, {
      method: method,
      headers: {
        'Authorization': ' Token ' + localStorage.getItem('token'),
        'Content-Type': 'application/json'
      }
    })
  }

  selectVariation(evt, variant){
    if(variant !== this.state.selectedVariant){
      //first handle border
      evt.target.parentNode.classList.add("selected-variation")
      const prevSelected = this.state.selectedVariant
      document.getElementById(prevSelected).childNodes[0].classList.remove("selected-variation")
      //now handle which fields to update
      const variantObject = this.state.variations[variant.split('-')[1]]
      this.setState({
        selectedVariant: variant,
        quantity: variantObject.hasOwnProperty('quantity') === true ? variantObject.quantity : '',
        wholesalePrice: variantObject.hasOwnProperty('wholesalePrice') === true ? variantObject.wholesalePrice : '',
        retailPrice: variantObject.hasOwnProperty('retailPrice') === true ? variantObject.retailPrice : '',
        webPrice: variantObject.hasOwnProperty('webPrice') === true ? variantObject.webPrice : '',
        casePackCount: variantObject.hasOwnProperty('casePackCount') === true ? variantObject.casePackCount : '',
        casePackDimensions: variantObject.hasOwnProperty('casePackDimensions') === true ? variantObject.casePackDimensions : '',
        casePackUpc: variantObject.hasOwnProperty('casePackUpc') === true ? variantObject.casePackUpc : '',
        sku: variantObject.hasOwnProperty('productSku') === true ? variantObject.productSku : this.state.upc,
        upc: variantObject.hasOwnProperty('productUpc') === true ? variantObject.productUpc : '',
        unitDimensions: variantObject.hasOwnProperty('unitDimensions') === true ? variantObject.unitDimensions : '',
        productImage: variantObject.hasOwnProperty('productImage') === true ? variantObject.productImage : ''
      })
    }
  }

  render(){
    return(
      <div id="product-page">
        <div className="col">
          <h2>{this.state.description}</h2>
          {this.state.variations &&
            <div className="row">
              {this.state.variations.map((v, i) => (
                <div className={`col`} id={`variation-${i}`} key={i}>
                  <div
                    className={i === 0 ? `card selected-variation` : `card`}
                    onClick={evt => this.selectVariation(evt, `variation-${i}`)}
                  >
                    <div className="card-content">
                      {v.quantity}
                    </div>
                  </div>
                </div>
              ))}
            </div>
          }

        </div>
        <div className="col s12">
          <p className="item-number"> <b>{this.state.sku}</b></p>
          {this.state.bulk &&
            <p>
              <b>Bulk:</b>
              <Link to={{
                pathname: `/faqs/bulk/${this.state.bulk}/`,
                state: { bulkId: this.state.bulk }
              }}>
                {this.state.bulkName}
              </Link>
            </p>
          }
        </div>

        <div className="col s12">
          <div className="general-information">
            <ul className="prices">
              <li>Wholesale Price: {this.state.wholesalePrice}</li>
              <li>Retail Price: {this.state.retailPrice}</li>
              {/* <li>Web: {this.state.webPrice}</li> */}
            </ul>
            <p><b>Storage Conditions:</b> {this.state.storageConditions}</p>
            <p><b>Case Pack Count:</b> {this.state.casePackCount}</p>
            <p><b>Case Pack UPC:</b> {this.state.casePackUpc}</p>
            <p><b>Case Pack Dimensions:</b> {this.state.casePackDimensions}</p>
            <p><b>Unit Dimensions:</b> {this.state.unitDimensions}</p>
            <p><b>UPC:</b> {this.state.upc}</p>
          </div>
          <br/>
          <div id="revision-information">
            <h5>Current Revision Label  <b className="item-number">#{this.state.currentRevisionLabelNumber}</b></h5>
          </div>

          <div id="image-area">
            {this.state.productImage !== undefined &&
              <div>
                <Modal
                  header={this.state.description}
                  fixedFooter
                  trigger=
                    {<span>
                      <img
                        className="thumbnail z-depth-1"
                        alt="product.product_image_thumbnail"
                        src={this.state.productImage}
                        style={{'width': '370px', 'cursor':'pointer'}}
                      />
                    </span>}
                >
                  <img
                    alt="product.product_image"
                    src={this.state.productImage}
                    style={{'width': '800px'}}
                  />
                </Modal>
              </div>
            }
          </div>

          <div id="note-area">{this.state.revisionNotes}</div>
          {localStorage.getItem('isLoggedIn') === "true" &&
            <Link
              to={{
                pathname: "/faqs/add-product",
                state: {
                  category: "General Product Information",
                  context: "Edit",
                  infoPageId: this.state.infoPageId,
                  pastRef: this.props.location.pathname,
                  id: this.state.id,
                  name: this.state.description,
                  productImage: this.state.productImage,
                  sku: this.state.sku,
                  productType: this.state.productType,
                  bulk: this.state.bulk,
                  wholesale: this.state.wholesalePrice,
                  retail: this.state.retailPrice,
                  web: this.state.webPrice,
                  storageConditions: this.state.storageConditions,
                  casePackCount: this.state.casePackCount,
                  casePackDimensions: this.state.casePackDimensions,
                  casePackUpc: this.state.casePackUpc,
                  unitDimensions: this.state.unitDimensions,
                  upc: this.state.upc,
                  quantity: this.state.quantity,
                  currentRevisionLabelNumber: this.state.currentRevisionLabelNumber,
                  revisionNotes: this.state.revisionNotes,
                  variations: this.state.variations && this.state.variations.length > 0 ? this.state.variations : [{}],
                  featured: this.state.featured,
                  featuredHeadline: this.state.featuredHeadline,
                  published: this.state.published
              }}}
            >
              <span className="waves-effect waves-light btn right">Edit General Info</span>
            </Link>
          }

          <div className="section-heading">
            <h4>General Information FAQs</h4>
            <FaqList
              faqs={this.state.generalInformationFaqs}
              nameRef={this.state.description}
              pastRef={this.props.location.pathname}
              prodIdRef={this.state.id}
              objectType="product"
            />
            {localStorage.getItem('isLoggedIn') === "true" &&
              <Link
                to={{
                  pathname: "/faqs/add-faq",
                  state: {
                    category: "generalInformation",
                    objectType: "product",
                    nameRef: this.state.description,
                    pastRef: this.props.location.pathname,
                    prodIdRef: this.state.id
                }}}
              >
                <span className="waves-effect waves-light btn right">Add Faq</span>
              </Link>
            }
          </div>

          <div className="section-heading">
            <h4>Safety & Dosage FAQs</h4>
            <FaqList
              faqs={this.state.safetyAndDosageFaqs}
              pastRef={this.props.location.pathname}
              nameRef={this.state.description}
              prodIdRef={this.state.id}
              objectType="product"
            />
            {localStorage.getItem('isLoggedIn') === "true" &&
              <Link
                to={{
                  pathname: "/faqs/add-faq",
                  state: {
                    category: "safetyAndDosage",
                    objectType: "product",
                    nameRef: this.state.description,
                    pastRef: this.props.location.pathname,
                    prodIdRef: this.state.id
                }}}
              >
                <span className="waves-effect waves-light btn right">Add Faq</span>
              </Link>
            }
          </div>
        </div>
      </div>
    )
  }
}

export default ProductPage
