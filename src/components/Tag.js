import React from 'react'

const Tag = ({tag}) => (
  <span className="key-term z-depth-2">
    {tag}
  </span>
)

export default Tag
