import React from 'react'
import axios from 'axios'
import moment from 'moment'
import { Link } from 'react-router-dom'


// const RecentlyModified = () => (
class FeaturedUpdates extends React.Component {
  constructor(){
    super()
    this.getFeaturedUpdates()

    this.state = {
      featuredUpdates: []
    }
  }

  getFeaturedUpdates() {
    axios.get('https://localhost:4430/faq/featured/')
    // axios.get('/faq/rec-mod/')
      .then(res => {
        this.setState({
          featuredUpdates: res.data
        }, () => {
          console.log(this.state.featuredUpdates)
        })
      })
  }

  render() {

    return(
      <div id="recently-modified">
        <h5 className="featured-updates-title">Featured Updates</h5>

          {this.state.featuredUpdates.map((featuredUpdate) => {
            let type = featuredUpdate.url.split('/')[2]
            // todo: add all the different type conversions here!!
            switch (type){
              case 'standard':
                type = 'nordic-standard'
                break;
            }

            const id = featuredUpdate.url.split('/')[3]
            const url = `/faqs/${type}/${id}`
            // <p key={recMod.name}><Link to={url}></Link>

            const name = featuredUpdate.name.split(':')
            const date = featuredUpdate.date_modified.split('-')
            const year = date[0]
            const month = date[1]
            const day = date[2].split('T')[0]
            const headline = featuredUpdate.featured_headline
            return (
              <Link to={url}>
                <div class="featured-update-item">
                  <p>{name[0]}: <span className="rec-mod-link">{name[1]}</span></p>
                  <p>{headline}</p>
                  <i>{month} / {day} / {year}</i>
                </div>
              </Link>
            )
          })}
      </div>
    )
  }
}

export default FeaturedUpdates
