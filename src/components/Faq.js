import React from 'react'
import { Link } from 'react-router-dom'

const Faq = ({question, answer, url, id, nameRef, pastRef, prodIdRef, objectType}) => (
  <div className="faq-item">
    {localStorage.getItem('isLoggedIn') === "true" &&
      <Link
        to={{
          pathname: "/faqs/add-faq",
          state: {
            id: id,
            url: url,
            question: question,
            answer: answer,
            context: 'edit',
            nameRef: nameRef,
            pastRef: pastRef,
            prodIdRef: prodIdRef,
            objectType: objectType
          }
      }}>
        <i className="fas fa-edit right"></i>
      </Link>
    }
    <h5>{question}</h5>
    <p dangerouslySetInnerHTML={{__html: answer}} />
  </div>
)

export default Faq
