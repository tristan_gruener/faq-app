import React from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'

class Search extends React.Component {
  constructor() {
    super()
    this.state = {
      searchInput: '',
      showResults: true,
      dataSet: [],
      searchResults: [],
      productsLoaded: false,
      standardsLoaded: false
    }

    this.getProducts()
    this.getIngredients()
    this.getBulks()
    this.getStandards()
  }

  getProducts() {


    /*
      PF-47
      expand dataset to include variations

      1 - when you do the get, you get published products
      2 - go through the published products and determine which products have variations
      3 - stash all of those links variationLinks into the dataSet.
          make them part of publishedProducts list before setting state

    */



    // axios.get('https://localhost:4430/faq/product/').then(res => {
    axios.get('/faq/product/').then(res => {
      const publishedProducts = []
      for (var i = 0; i < res.data.length; i++) {
        if (res.data[i].published === true) {

          publishedProducts.push({
            name: res.data[i].description,
            sku: res.data[i].sku,
            type: 'Product',
            link: `/faqs/product/${res.data[i].id}`,
            id: res.data[i].id
          })

          const variationObject = localStorage.getItem(`product-with-variant-${res.data[i].id}`)

          if(variationObject !== null){
            const variants = JSON.parse(variationObject)
            variants.map((variant) => {
                publishedProducts.push({
                  name: variant.quantity,
                  sku: variant.hasOwnProperty('productSku') === true ? variant.productSku : '',
                  type: 'Product',
                  link: `/faqs/product/${res.data[i].id}`,
                  id: res.data[i].id
                })
            })
          }
        }
      }

      const dataSet = this.state.dataSet
      this.setState({
        dataSet: dataSet.concat(publishedProducts),
        productsLoaded: true
      })
    })
  }

  getIngredients() {
    // axios.get('https://localhost:4430/faq/ingredient/').then(res => {
    axios.get('/faq/ingredient/').then(res => {
      const publishedIngredients = []
      for (var i = 0; i < res.data.length; i++) {
        if (res.data[i].published === true) {
          publishedIngredients.push({
            name: res.data[i].description,
            type: 'Raw Material',
            sku: '',
            link: `/faqs/ingredient/${res.data[i].id}`,
            id: res.data[i].id
          })
        }
      }

      const dataSet = this.state.dataSet
      this.setState({
        dataSet: dataSet.concat(publishedIngredients),
        ingredientsLoaded: true
      })
    })
  }

  getBulks() {
    // axios.get('https://localhost:4430/faq/bulk/').then(res => {
    axios.get('/faq/bulk/').then(res => {
      const publishedBulks = []
      for (var i = 0; i < res.data.length; i++) {
        if (res.data[i].published === true) {
          publishedBulks.push({
            name: res.data[i].name,
            type: 'Bulk',
            sku: '',
            link: `/faqs/bulk/${res.data[i].id}`,
            id: res.data[i].id
          })
        }
      }

      const dataSet = this.state.dataSet
      this.setState({
        dataSet: dataSet.concat(publishedBulks),
        bulksLoaded: true
      })
    })
  }

  getStandards() {
    // axios.get('https://localhost:4430/faq/standard/').then(res => {
    axios.get('/faq/standard/').then(res => {
      const publishedStandards = []
      for (var i = 0; i < res.data.length; i++) {
        if (res.data[i].published === true) {
          publishedStandards.push({
            name: res.data[i].name,
            type: 'Nordic Standard',
            sku: '',
            link: `/faqs/nordic-standard/${res.data[i].id}`,
            id: res.data[i].id
          })
        }
      }

      const dataSet = this.state.dataSet
      this.setState({
        dataSet: dataSet.concat(publishedStandards),
        standardsLoaded: true
      })
    })
  }

  updateSearchInput(evt){
    const searchText = evt.target.value.toLowerCase()
    const searchResults = []

    for (let i = 0; i < this.state.dataSet.length; i++){
      if (this.state.dataSet[i].name.toLowerCase().indexOf(searchText) > -1 || this.state.dataSet[i].sku.toLowerCase().indexOf(searchText) > -1){
        searchResults.push(this.state.dataSet[i])
      }
    }

    if(evt.target.value.length > 0){
      this.setState({
        showResults: true,
        searchInput: evt.target.value,
        searchResults: searchResults
      })
    } else {
      this.setState({
        showResults: false,
        searchInput: evt.target.value,
        searchResults: searchResults
      })
    }
  }

  render() {
    return (
      <form>
        <div className="search-form">
          <div className="input-field">
            <input
              disabled={!this.state.productsLoaded}
              id="icon_prefix"
              type="text"
              className="validate"
              value={this.state.searchInput}
              onChange={evt => this.updateSearchInput(evt)}
            />
            <label htmlFor="icon_prefix"><i className="fas fa-search" /> Search Products, Raw Materials, or Bulks</label>
          </div>
        </div>
        {
          this.state.showResults &&
            <div>
              {this.state.searchResults.map((item, i) => {
                return (
                  <Link
                    to={{
                      pathname: item.link,
                      state: {
                        prodId: item.id,
                        bulkId: item.id,
                        ingredientId: item.id
                      }
                    }}
                    className="result-link"
                    key={i}
                  >
                    <div className="search-result-label">{item.type}</div>
                    <h5 className="search-result">{item.sku !== '' && <span>{item.sku} |</span>}  {item.name}</h5>
                  </Link>
                )
              })}
            </div>
        }
      </form>
    )
  }
}

export default Search
