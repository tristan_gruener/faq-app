import React from 'react'
import { Input } from 'react-materialize'
import _ from 'lodash'

class AddProduct extends React.Component {
  constructor(props){
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.setProdType = this.setProdType.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.addNewVariation = this.addNewVariation.bind(this)
    this.getBulks = this.getBulks.bind(this)

    if(_.isEmpty(this.props.location.state)){
      this.state = {
        redirectDelay: false,
        context: 'Add',
        productName: '',
        productImage: '',
        quantity: '',
        productSku: '',
        productUpc: '',
        casePackUpc: '',
        wholesalePrice: '',
        retailPrice: '',
        webPrice: '',
        storageConditions: '',
        casePackCount: '',
        casePackDimensions: '',
        unitDimensions: '',
        currentRevisionLabelNumber: '',
        bulkType: '',
        bulkNames: ['a', 'b'],
        variations: [{}],
        selectedVariant: 'variation-0',
        previousVariant: '',
        featured: false,
        featuredHeadline: ''
      }
    }else{
      const variantObject = []
      //if variation object NOT present
      if (this.props.location.state.variations.length === 1) {
        this.props.location.state.variations.map(variation => (
          variantObject.push({
            quantity: variation.hasOwnProperty('quantity') === true ? variation.quantity : this.props.location.state.quantity,
            wholesalePrice: variation.hasOwnProperty('wholesalePrice') === true ? variation.wholesalePrice : this.props.location.state.wholesale,
            retailPrice: variation.hasOwnProperty('retailPrice') === true ? variation.retailPrice : this.props.location.state.retail,
            webPrice: variation.hasOwnProperty('webPrice') === true ? variation.webPrice : this.props.location.state.web,
            casePackCount: variation.hasOwnProperty('casePackCount') === true ? variation.casePackCount : this.props.location.state.casePackCount,
            casePackDimensions: variation.hasOwnProperty('casePackDimensions') === true ? variation.casePackDimensions : this.props.location.state.casePackDimensions,
            casePackUpc: variation.hasOwnProperty('casePackUpc') === true ? variation.casePackUpc : this.props.location.state.casePackUpc,
            productSku: variation.hasOwnProperty('productSku') === true ? variation.productSku : this.props.location.state.sku,
            productUpc: variation.hasOwnProperty('productUpc') === true ? variation.productUpc : this.props.location.state.upc,
            unitDimensions: variation.hasOwnProperty('unitDimensions') === true ? variation.unitDimensions : this.props.location.state.unitDimensions,
            productImage: variation.hasOwnProperty('productImage') === true ? variation.productImage : this.props.location.state.productImage
          })
        ))
      } else {
        this.props.location.state.variations.map(variation => (
          variantObject.push({
            quantity: variation.hasOwnProperty('quantity') === true ? variation.quantity : '',
            wholesalePrice: variation.hasOwnProperty('wholesalePrice') === true ? variation.wholesalePrice : '',
            retailPrice: variation.hasOwnProperty('retailPrice') === true ? variation.retailPrice : '',
            webPrice: variation.hasOwnProperty('webPrice') === true ? variation.webPrice : '',
            casePackCount: variation.hasOwnProperty('casePackCount') === true ? variation.casePackCount : '',
            casePackDimensions: variation.hasOwnProperty('casePackDimensions') === true ? variation.casePackDimensions : '',
            casePackUpc: variation.hasOwnProperty('casePackUpc') === true ? variation.casePackUpc : '',
            productSku: variation.hasOwnProperty('productSku') === true ? variation.productSku : '',
            productUpc: variation.hasOwnProperty('productUpc') === true ? variation.productUpc : '',
            unitDimensions: variation.hasOwnProperty('unitDimensions') === true ? variation.unitDimensions : '',
            productImage: variation.hasOwnProperty('productImage') === true ? variation.productImage : ''

          })
        ))
      }

      const bulkyBulk = `/faq/bulk/${this.props.location.state.bulk}/`
      // console.log(bulkyBulk)

      this.state = {
        redirectDelay: true,
        category: this.props.location.state.category,
        context: this.props.location.state.context,
        prodId: this.props.location.state.id,
        infoPageId: this.props.location.state.infoPageId,
        productName: this.props.location.state.name,

        // productImage: variantObject[0].hasOwnProperty('productImage') === true ? variantObject[0].productImage : this.props.location.state.productImage,
        bulkType: `/faq/bulk/${this.props.location.state.bulk}/`,
        // bulkType: this.props.location.state.bulk,
        prodType: this.setProdType(this.props.location.state.productType),
        storageConditions: this.props.location.state.storageConditions,
        currentRevisionLabelNumber: this.props.location.state.currentRevisionLabelNumber,
        revisionNotes: this.props.location.state.revisionNotes,
        published: this.props.location.state.published === true ? 1 : 0,
        bulkNames: ['a', 'b'],
        variations: variantObject,
        selectedVariant: 'variation-0',
        previousVariant: '',
        quantity: variantObject[0].hasOwnProperty('quantity') === true ? variantObject[0].quantity : this.props.location.state.quantity,
        wholesalePrice: variantObject[0].hasOwnProperty('wholesalePrice') === true ? variantObject[0].wholesalePrice : this.props.location.state.wholesale,
        retailPrice: variantObject[0].hasOwnProperty('retailPrice') === true ? variantObject[0].retailPrice : this.props.location.state.retail,
        webPrice: variantObject[0].hasOwnProperty('webPrice') === true ? variantObject[0].webPrice : this.props.location.state.web,
        casePackCount: variantObject[0].hasOwnProperty('casePackCount') === true ? variantObject[0].casePackCount : this.props.location.state.casePackCount,
        casePackDimensions: variantObject[0].hasOwnProperty('casePackDimensions') === true ? variantObject[0].casePackDimensions : this.props.location.state.casePackDimensions,
        casePackUpc: variantObject[0].hasOwnProperty('casePackUpc') === true ? variantObject[0].casePackUpc : this.props.location.state.casePackUpc,
        productSku: variantObject[0].hasOwnProperty('productSku') === true ? variantObject[0].productSku : this.props.location.state.sku,
        productUpc: variantObject[0].hasOwnProperty('productUpc') === true ? variantObject[0].productUpc : this.props.location.state.upc,
        unitDimensions: variantObject[0].hasOwnProperty('unitDimensions') === true ? variantObject[0].unitDimensions : this.props.location.state.unitDimensions,
        productImage: variantObject[0].hasOwnProperty('productImage') === true ? variantObject[0].productImage : this.props.location.state.productImage,
        featured: this.props.location.state.featured,
        featuredHeadline: this.props.location.state.featuredHeadline
      }
    }
    this.getBulks()
  }

  setProdType(prodTypeUrl){
    const typeId = parseInt(prodTypeUrl.split('/')[5])
    let prodTypeRef = 'null'
    switch(typeId){
      case 4:
        prodTypeRef = '/faq/product-type/4/'
        break
      case 5:
        prodTypeRef = '/faq/product-type/5/'
        break
      case 6:
        prodTypeRef = '/faq/product-type/6/'
        break
      case 7:
        prodTypeRef = '/faq/product-type/7/'
        break
      default:
        prodTypeRef = 'null'
    }
    return prodTypeRef
  }

  handleSubmit(e){
    e.preventDefault()
    const productName = e.target.elements.productName.value
    const productImage = e.target.elements.productImage.value
    // const productSku = e.target.elements.productSku.value
    const published = e.target.elements.published.value
    const productType = e.target.elements.productType.value
    // const productUpc = e.target.elements.productUpc.value
    // const casePackUpc = e.target.elements.casePackUpc.value
    // const wholesalePrice = e.target.elements.wholesalePrice.value
    // const retailPrice = e.target.elements.retailPrice.value
    // const webPrice = e.target.elements.webPrice.value
    const storageConditions = e.target.elements.storageConditions.value
    // const casePackCount = e.target.elements.casePackCount.value
    // const casePackDimensions = e.target.elements.casePackDimensions.value
    // const unitDimensions = e.target.elements.unitDimensions.value
    const currentRevisionLabelNumber = e.target.elements.currentRevisionLabelNumber.value
    const revisionNotes = e.target.elements.revisionNotes.value
    const bulkType = e.target.elements.bulkType.value

    const featured = e.target.elements.featured.value
    const featuredHeadline = e.target.elements.featuredHeadline.value

    let url = '/faq/product/'
    let method = 'POST'
    let data = {}
    let infoPageData = {}
    //ADD
    if(this.state.context === 'Add'){
      infoPageData = {
        generalInformation: {
          'storageConditions': storageConditions,
          'casePackCount': this.state.variations[0].hasOwnProperty('casePackCount') === true ? this.state.variations[0].casePackCount : '',
          'casePackDimensions': this.state.variations[0].hasOwnProperty('casePackDimensions') === true ? this.state.variations[0].casePackDimensions : '',
          'unitDimensions': this.state.variations[0].hasOwnProperty('unitDimensions') === true ? this.state.variations[0].unitDimensions : '',
          'upc': this.state.variations[0].hasOwnProperty('productUpc') === true ? this.state.variations[0].productUpc : '',
          'casePackUpc': this.state.variations[0].hasOwnProperty('casePackUpc') === true ? this.state.variations[0].casePackUpc : '',
          'wholesalePrice': this.state.variations[0].hasOwnProperty('wholesalePrice') === true ? this.state.variations[0].wholesalePrice : '',
          'retailPrice': this.state.variations[0].hasOwnProperty('retailPrice') === true ? this.state.variations[0].retailPrice : '',
          'webPrice': this.state.variations[0].hasOwnProperty('webPrice') === true ? this.state.variations[0].webPrice : '',
          // 'productImage': productImage
          'productImage': this.state.variations[0].hasOwnProperty('productImage') === true ? this.state.variations[0].productImage : ''
        },
        revisionInformation: {
          'currentRevisionLabelNumber': currentRevisionLabelNumber,
          'revisionNotes': revisionNotes
        },
        variations: this.state.variations
      }

      data = {
        'description': productName,
        'sku': this.state.variations[0].hasOwnProperty('productSku') === true ? this.state.variations[0].productSku : '',
        'published': published,
        'product_type': productType,
        'bulk': bulkType,
        'faqs': [],
        'info_pages': [{
          'html_j': JSON.stringify(infoPageData)
        }],
        'featured': featured,
        'featured_headline': featuredHeadline
      }
      this.pushToDb(url, method, data, 'add')
    }else{
      method = 'PUT'
      //EDIT
      //first edit product-----------------------------------
      data = {
        'description': productName,
        'sku': this.state.variations[0].hasOwnProperty('productSku') === true ? this.state.variations[0].productSku : '',
        'published': published,
        'product_type': productType,
        'bulk': bulkType,
        'faqs': [],
        'info_pages': [],
        'featured': featured,
        'featured_headline': featuredHeadline
      }

      url = `/faq/product/${this.state.prodId}/`
      this.pushToDb(url, method, data, 'edit', 'step-1')
      //---------------------------------------------------
      //now edit infopage
      infoPageData = {
        generalInformation: {
          'storageConditions': storageConditions,
          'casePackCount': this.state.variations[0].hasOwnProperty('casePackCount') === true ? this.state.variations[0].casePackCount : '',
          'casePackDimensions': this.state.variations[0].hasOwnProperty('casePackDimensions') === true ? this.state.variations[0].casePackDimensions : '',
          'unitDimensions': this.state.variations[0].hasOwnProperty('unitDimensions') === true ? this.state.variations[0].unitDimensions : '',
          'upc': this.state.variations[0].hasOwnProperty('productUpc') === true ? this.state.variations[0].productUpc : '',
          'casePackUpc': this.state.variations[0].hasOwnProperty('casePackUpc') === true ? this.state.variations[0].casePackUpc : '',
          'wholesalePrice': this.state.variations[0].hasOwnProperty('wholesalePrice') === true ? this.state.variations[0].wholesalePrice : '',
          'retailPrice': this.state.variations[0].hasOwnProperty('retailPrice') === true ? this.state.variations[0].retailPrice : '',
          'webPrice': this.state.variations[0].hasOwnProperty('webPrice') === true ? this.state.variations[0].webPrice : '',
          'productImage': this.state.variations[0].hasOwnProperty('productImage') === true ? this.state.variations[0].productImage : ''
        },
        revisionInformation: {
          'currentRevisionLabelNumber': currentRevisionLabelNumber,
          'revisionNotes': revisionNotes
        },
        variations: this.state.variations
      }

      data = {
        'html_j': JSON.stringify(infoPageData)
      }

      url = `/faq/info-page/${this.state.infoPageId}/`
      this.pushToDb(url, method, data, 'edit', 'step-2')
    }
  }

  pushToDb(url, method, data, delay, step){
    fetch(url, {
      method: method,
      body: JSON.stringify(data),
      headers: {
        'Authorization': ' Token ' + localStorage.getItem('token'),
        'Content-Type': 'application/json'
      }
    }).then(res => res.json()).then(response => {

      localStorage.setItem(`product-${response.id}`, response.bulk)

      if(delay === 'add') {
         alert('succesfully added item')
         const variants = JSON.parse(response.info_pages[0].html_j).variations
         if(JSON.parse(variants.length > 1)){
           localStorage.setItem(`product-with-variant-${response.id}`, JSON.stringify(variants))
         }
         const url = `/faqs/product/${response.id}/`
         this.props.history.push(url)
         window.scrollTo(0,0)
      }

      if(delay === 'edit') {
        if (step === 'step-1'){
          alert('successfully edited item')
          const variants = JSON.parse(response.info_pages[0].html_j).variations
          if(JSON.parse(variants.length > 1)){
            localStorage.setItem(`product-with-variant-${response.id}`, JSON.stringify(variants))
          }
          const url = `/faqs/product/${response.id}/`
          this.props.history.push(url)
          window.scrollTo(0,0)
        }
      }


      // if(this.state.redirectDelay === true) {
      //   if(delay === 0) {
      //     alert("success base save");
      //   }
      //
      //   if(delay === 1) {
      //     alert("success second save")
      //     const url = `/faqs/product/${response.id}/`
      //     this.props.history.push(url)
      //     window.scrollTo(0,0)
      //   }
      // } else {
      //   const url = `/faqs/product/${response.id}/`
      //   this.props.history.push(url)
      //   window.scrollTo(0,0)
      // }

    })
  }

  handleDelete(){
    const url = `/faq/product/${this.state.prodId}/`
    const deleteConfirmation = window.confirm("Are you sure you want to delete this product? (all the faqs that have been added will also be deleted)");
    if (deleteConfirmation) {
      fetch(url, {
        method: 'DELETE',
        headers: {
          'Authorization': ' Token ' + localStorage.getItem('token'),
          'Content-Type': 'application/json'
        }
      }).then(() => {
        window.location.assign('/faqs/');
      })
    }
  }

  getBulks(){
    fetch(`/faq/bulk/`).then((res) => res.json()).then((json) => {
    // fetch(`https://localhost:4430/faq/bulk/`).then((res) => res.json()).then((json) => {
      const sortedBulks = _.orderBy(json, [bulk => bulk.name], ['asc'])
      this.setState({
        bulkNames: sortedBulks
      })
    })
  }

  updateProductName(evt){
    this.setState({
      productName: evt.target.value
    });
  }

  updateProductImage(evt){
    const variations = this.state.variations
    const variationId = this.state.selectedVariant.split('-')[1]
    variations[variationId].productImage = evt.target.value
    this.setState({
      productImage: evt.target.value
    });
  }

  updateProductSku(evt){
    const variations = this.state.variations
    const variationId = this.state.selectedVariant.split('-')[1]
    variations[variationId].productSku = evt.target.value
    this.setState({
      productSku: evt.target.value
    });
  }

  updateProductUpc(evt){
    const variations = this.state.variations
    const variationId = this.state.selectedVariant.split('-')[1]
    variations[variationId].productUpc = evt.target.value
    this.setState({
      productUpc: evt.target.value
    });
  }

  updateQuantity(evt){
    const variations = this.state.variations
    const variationId = this.state.selectedVariant.split('-')[1]
    variations[variationId].quantity = evt.target.value
    this.setState({
      quantity: evt.target.value
    })
  }

  updateWholesalePrice(evt){
    const variations = this.state.variations
    const variationId = this.state.selectedVariant.split('-')[1]
    variations[variationId].wholesalePrice = evt.target.value
    this.setState({
      wholesalePrice: evt.target.value
    });
  }

  updateRetailPrice(evt){
    const variations = this.state.variations
    const variationId = this.state.selectedVariant.split('-')[1]
    variations[variationId].retailPrice = evt.target.value
    this.setState({
      retailPrice: evt.target.value
    });
  }

  updateWebPrice(evt){
    const variations = this.state.variations
    const variationId = this.state.selectedVariant.split('-')[1]
    variations[variationId].webPrice = evt.target.value
    this.setState({
      webPrice: evt.target.value
    });
  }

  updateStorageConditions(evt){
    this.setState({
      storageConditions: evt.target.value
    });
  }

  updateCasePackCount(evt){
    const variations = this.state.variations
    const variationId = this.state.selectedVariant.split('-')[1]
    variations[variationId].casePackCount = evt.target.value
    this.setState({
      casePackCount: evt.target.value
    });
  }

  updateCasePackDimensions(evt){
    const variations = this.state.variations
    const variationId = this.state.selectedVariant.split('-')[1]
    variations[variationId].casePackDimensions = evt.target.value
    this.setState({
      casePackDimensions: evt.target.value
    });
  }

  updateCasePackUpc(evt) {
    const variations = this.state.variations
    const variationId = this.state.selectedVariant.split('-')[1]
    variations[variationId].casePackUpc = evt.target.value
    this.setState({
      casePackUpc: evt.target.value
    })
  }

  updateUnitDimensions(evt){
    const variations = this.state.variations
    const variationId = this.state.selectedVariant.split('-')[1]
    variations[variationId].unitDimensions = evt.target.value
    this.setState({
      unitDimensions: evt.target.value
    });
  }

  updatePublished(evt){
    this.setState({
      published: evt.target.value
    });
  }

  updateFeatured(evt){
    this.setState({
      featured: evt.target.value
    });
  }

  updateFeaturedHeadline(evt){
    this.setState({
      featuredHeadline: evt.target.value
    });
  }

  updateProductType(evt){
    this.setState({
      prodType: evt.target.value
    })
  }


  /*
    UPDATE_BULK_TYPE /////////////////////////////////////////////////////////////////////////
  */


  updateBulkType(evt){

    // const bulkType = `/faq/bulk/${evt.target.value}/`
    // console.log(bulkType)
    this.setState({
      // bulkType: bulkType
      bulkType: evt.target.value
    })
  }

  updateRevisionLabelNumber(evt){
    this.setState({
      currentRevisionLabelNumber: evt.target.value
    })
  }

  updateRevisionNotes(evt){
    this.setState({
      revisionNotes: evt.target.value
    })
  }

  selectVariation(evt, variant){
    if(variant !== this.state.selectedVariant){
      //first handle border
      evt.target.parentNode.classList.add("selected-variation")
      const prevSelected = this.state.selectedVariant
      document.getElementById(prevSelected).childNodes[0].classList.remove("selected-variation")
      //now handle which fields to update
      const variantObject = this.state.variations[variant.split('-')[1]]
      this.setState({
        selectedVariant: variant,
        quantity: variantObject.hasOwnProperty('quantity') === true ? variantObject.quantity : '',
        wholesalePrice: variantObject.hasOwnProperty('wholesalePrice') === true ? variantObject.wholesalePrice : '',
        retailPrice: variantObject.hasOwnProperty('retailPrice') === true ? variantObject.retailPrice : '',
        webPrice: variantObject.hasOwnProperty('webPrice') === true ? variantObject.webPrice : '',
        casePackCount: variantObject.hasOwnProperty('casePackCount') === true ? variantObject.casePackCount : '',
        casePackDimensions: variantObject.hasOwnProperty('casePackDimensions') === true ? variantObject.casePackDimensions : '',
        casePackUpc: variantObject.hasOwnProperty('casePackUpc') === true ? variantObject.casePackUpc : '',
        productSku: variantObject.hasOwnProperty('productSku') === true ? variantObject.productSku : '',
        productUpc: variantObject.hasOwnProperty('productUpc') === true ? variantObject.productUpc : '',
        unitDimensions: variantObject.hasOwnProperty('unitDimensions') === true ? variantObject.unitDimensions : '',
        productImage: variantObject.hasOwnProperty('productImage') === true ? variantObject.productImage : ''
      })
    }
  }

  addNewVariation(){
    var temp = this.state.variations
    temp.push({})
    this.setState({
      variation: temp
    })
  }

  deleteVariation(e, variant) {
    e.preventDefault()

    const deleteConfirmation = window.confirm("Are you sure you want to delete this product variation?");
    if (deleteConfirmation) {
      const variantId = variant.split('-')[1].toString()
      var variantObject = this.state.variations
      variantObject.splice(variantId, 1)


      const prevSelected = this.state.selectedVariant
      document.getElementById(prevSelected).childNodes[0].classList.remove("selected-variation")
      document.getElementById('variation-0').childNodes[0].classList.add("selected-variation")



      this.setState({
        variations: variantObject,
        selectedVariant: 'variation-0',
        quantity: variantObject[0].hasOwnProperty('quantity') === true ? variantObject[0].quantity : '',
        wholesalePrice: variantObject[0].hasOwnProperty('wholesalePrice') === true ? variantObject[0].wholesalePrice : '',
        retailPrice: variantObject[0].hasOwnProperty('retailPrice') === true ? variantObject[0].retailPrice : '',
        webPrice: variantObject[0].hasOwnProperty('webPrice') === true ? variantObject[0].webPrice : '',
        casePackCount: variantObject[0].hasOwnProperty('casePackCount') === true ? variantObject[0].casePackCount : '',
        casePackDimensions: variantObject[0].hasOwnProperty('casePackDimensions') === true ? variantObject[0].casePackDimensions : '',
        casePackUpc: variantObject[0].hasOwnProperty('casePackUpc') === true ? variantObject[0].casePackUpc : '',
        productSku: variantObject[0].hasOwnProperty('productSku') === true ? variantObject[0].productSku : '',
        productUpc: variantObject[0].hasOwnProperty('productUpc') === true ? variantObject[0].productUpc : '',
        unitDimensions: variantObject[0].hasOwnProperty('unitDimensions') === true ? variantObject[0].unitDimensions : '',
        productImage: variantObject[0].hasOwnProperty('productImage') === true ? variantObject[0].productImage : ''
      })
    }
  }

  render(){
    return(
      <div>
        {this.state.context === 'Edit' ? <h2>Edit</h2> : <h2>Add Product</h2>}
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="input-field col s10">
              <input
                id="product-name"
                type="text"
                className="validate"
                name="productName"
                value={this.state.productName}
                onChange={evt => this.updateProductName(evt)}
              />
              <label htmlFor="product-name" className={this.state.context === 'Edit' && 'active'}>Product Name</label>
            </div>
          </div>

          <div className="row">
            <div className="input-field col s7">
              <input
                id="product-image"
                type="text"
                className="validate"
                style={{"fontSize":"12px"}}
                name="productImage"
                value={this.state.productImage}
                onChange={evt => this.updateProductImage(evt)}
              />
              <label htmlFor="product-image" className={this.state.context === 'Edit' && 'active'}>Product Image Url</label>
            </div>

            <div className="col s3">
              <img src={this.state.productImage} alt="product" style={{"width": "300px"}}/>
            </div>
          </div>

          <div className="row">
            <Input
              s={4}
              type='select'
              label="Product Type"
              id="productType"
              className="productType"
              name="productType"
              value={this.state.prodType}
              onChange={evt => this.updateProductType(evt)}
            >
              <option value='/faq/product-type/4/'>Omega-3s</option>
              <option value='/faq/product-type/5/'>Probiotics</option>
              <option value='/faq/product-type/6/'>Vitamins & More</option>
              <option value='/faq/product-type/7/'>Gummies</option>
              <option value='null'>Uncategorized</option>
            </Input>
          </div>

          {/*
            UPDATE_BULK_TYPE /////////////////////////////////////////////////////////////////////////
          */}

          <h4>Bulk</h4>

          <div className="row">
            <Input
              s={6}
              type='select'
              label="Bulk"
              id="bulkType"
              className="bulkType"
              name="bulkType"
              // value={`/faq/bulk/${this.state.bulkType}/`}
              value={this.state.bulkType}
              onChange={evt => this.updateBulkType(evt)}
            >
              {this.state.bulkNames.map(bulk => {
                const bulkIdRef = `/faq/bulk/${bulk.id}/`
                return <option value={bulkIdRef} key={bulk.id}>{bulk.name}</option>
              })}
            </Input>
          </div>

          <h4>Available In</h4>

          <div className="row">
            <div className="input-field col s6">
              <input
                id="quantity"
                type="text"
                className="validate"
                name="quantity"
                value={this.state.quantity}
                onChange={evt => this.updateQuantity(evt)}
              />
              <label htmlFor="quantity" className={this.state.context === 'Edit' && 'active'}>Variation Name / Quantity (60 ct, 8 oz, etc.)</label>
            </div>
          </div>

          {this.state.variations &&
            <div>
              <div className="row">
                {this.state.variations.map((v, i) => (
                  <div className={`col`}>
                    <div id={`variation-${i}`} key={i}>
                      <div
                        className={i === 0 ? `card selected-variation` : `card`}
                        onClick={evt => this.selectVariation(evt, `variation-${i}`)}
                      >
                        <div className="card-content">
                          {v.quantity}
                        </div>
                      </div>
                    </div>
                    {i > 0 && <div className="delete-variant" onClick={evt => this.deleteVariation(evt, `variation-${i}`)}>X</div>}
                  </div>
                ))}
                <div className="col">
                  <div style={{"color":"#0277bd"}} onClick={this.addNewVariation}>
                    <i class="fas fa-plus" style={{"fontSize": "12px", "height": "0px"}}></i> Variation
                  </div>
                </div>
              </div>
            </div>
          }

          <h4>General Information for {this.state.quantity}</h4>

          <div className="row">
            <div className="input-field col s3">
              <input
                id="wholesale-price"
                type="text"
                className="validate"
                name="wholesalePrice"
                value={this.state.wholesalePrice}
                onChange={evt => this.updateWholesalePrice(evt)}
              />
              <label htmlFor="wholesale-price" className={this.state.context === 'Edit' && 'active'}>Wholesale Price</label>
            </div>
            <div className="input-field col s3">
              <input
                id="retail-price"
                type="text"
                className="validate"
                name="retailPrice"
                value={this.state.retailPrice}
                onChange={evt => this.updateRetailPrice(evt)}
              />
              <label htmlFor="retail-price" className={this.state.context === 'Edit' && 'active'}>Retail Price</label>
            </div>
            {/* <div className="input-field col s3">
              <input
                id="web-price"
                type="text"
                className="validate"
                name="webPrice"
                value={this.state.webPrice}
                onChange={evt => this.updateWebPrice(evt)}
              />
              <label htmlFor="web-price" className={this.state.context === 'Edit' && 'active'}>Web Price</label>
            </div> */}
          </div>

          <div className="row">
            <div className="input-field col s5">
              <input
                id="product-sku"
                type="text"
                className="validate"
                name="productSku"
                value={this.state.productSku}
                onChange={evt => this.updateProductSku(evt)}
              />
              <label htmlFor="product-sku" className={this.state.context === 'Edit' && 'active'}>SKU</label>
            </div>

            <div className="input-field col s5">
              <input
                id="product-upc"
                type="text"
                className="validate"
                name="productUpc"
                value={this.state.productUpc}
                onChange={evt => this.updateProductUpc(evt)}
              />
              <label htmlFor="product-upc" className={this.state.context === 'Edit' && 'active'}>UPC</label>
            </div>
          </div>

          <div className="row">
            <div className="input-field col s3">
              <input
                id="case-pack-count"
                type="text"
                className="validate"
                name="casePackCount"
                value={this.state.casePackCount}
                onChange={evt => this.updateCasePackCount(evt)}
              />
              <label htmlFor="case-pack-count" className={this.state.context === 'Edit' && 'active'}>Case Pack Count</label>
            </div>
            <div className="input-field col s3">
              <input
                id="case-pack-dimensions"
                type="text"
                className="validate"
                name="casePackDimensions"
                value={this.state.casePackDimensions}
                onChange={evt => this.updateCasePackDimensions(evt)}
              />
              <label htmlFor="case-pack-dimensions" className={this.state.context === 'Edit' && 'active'}>Case Pack Dimensions</label>
            </div>
            <div className="input-field col s3">
              <input
                id="case-pack-upc"
                type="text"
                className="validate"
                name="casePackUpc"
                value={this.state.casePackUpc}
                onChange={evt => this.updateCasePackUpc(evt)}
              />
              <label htmlFor="case-pack-upc" className={this.state.context === 'Edit' && 'active'}>Case Pack UPC</label>
            </div>
            <div className="input-field col s4">
              <input
                id="unit-dimensions"
                type="text"
                className="validate"
                name="unitDimensions"
                value={this.state.unitDimensions}
                onChange={evt => this.updateUnitDimensions(evt)}
              />
              <label htmlFor="unit-dimensions" className={this.state.context === 'Edit' && 'active'}>Unit Dimensions</label>
            </div>
          </div>

          <div className="row">
            <div className="input-field col s10">
              <input
                id="storage-conditions"
                type="text"
                className="validate"
                name="storageConditions"
                value={this.state.storageConditions}
                onChange={evt => this.updateStorageConditions(evt)}
              />
              <label htmlFor="storage-conditions" className={this.state.context === 'Edit' && 'active'}>Storage Conditions</label>
            </div>
          </div>

          <h4>Revision Information</h4>

          <div className="row">
            <div className="input-field col s6">
              <input
                id="current-revision-label-number"
                type="text"
                className="validate"
                name="currentRevisionLabelNumber"
                value={this.state.currentRevisionLabelNumber}
                onChange={evt => this.updateRevisionLabelNumber(evt)}
              />
              <label htmlFor="current-revision-label-number" className={this.state.context === 'Edit' && 'active'}>Current Revision Label Number</label>
            </div>
          </div>

          <div className="row">
            <div className="input-field col s8">
              <textarea
                id="revision-notes"
                type="text"
                className="validate materialize-textarea"
                name="revisionNotes"
                value={this.state.revisionNotes}
                onChange={evt => this.updateRevisionNotes(evt)}
              />
              <label htmlFor="revision-notes" className={this.state.context === 'Edit' && 'active'}>Revision Notes</label>
            </div>
          </div>

          <div className="row">
            <Input
              s={4}
              type='select'
              label="Published"
              id="published"
              className="published"
              name="published"
              value={this.state.published}
              onChange={evt => this.updatePublished(evt)}
            >
              <option value='0'>Unpublished</option>
              <option value='1'>Published</option>
            </Input>
          </div>

          <div className="row">
            <Input
              s={4}
              type='select'
              label="Featured"
              id="featured"
              className="featured"
              name="featured"
              value={this.state.featured}
              onChange={evt => this.updateFeatured(evt)}
            >
              <option value='false'>Not Featured</option>
              <option value='true'>Featured</option>
            </Input>
          </div>

          <div className="row">
            <div className="input-field col s10">
              <input
                id="featured-headline"
                type="text"
                className="featured-headline"
                name="featuredHeadline"
                value={this.state.featuredHeadline}
                onChange={evt => this.updateFeaturedHeadline(evt)}
              />
              <label htmlFor="featured-headline" className={this.state.context === 'Edit' && 'active'}>Featured Headline</label>
            </div>
          </div>

          <button className="btn confirm modal-action modal-close right">
            {this.state.context === 'Edit' ? <span>Update Product</span> : <span>Add Product</span>}
          </button>

          <br />
          <br />
          <br />

          {this.state.context === 'Edit' &&
            <button
              className="btn delete modal-action modal-close right"
              type="button"
              onClick={this.handleDelete}
            >
              Delete
            </button>
          }
        </form>
      </div>
    )
  }
}

export default AddProduct
