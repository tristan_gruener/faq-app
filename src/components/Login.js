import React from 'react'

class Login extends React.Component {
  constructor(props){
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.state = {
      error: ''
    }
  }

  handleSubmit(e){
    e.preventDefault()
    const username = e.target.elements.username.value
    const password = e.target.elements.password.value

    const url = '/faq/login/'
    const creds = {
      username: username,
      password: password
    }

    //so here lets show a popup if the login was succesful

    fetch(url, {
      method: 'POST',
      body: JSON.stringify(creds),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(response => {

        // console.log('response')
        // console.log(response)
        if (response.error === "Invalid Credentials") {
          this.setState({
            error: "Invalid Credentials"
          })
        } else if (response.error === "Please provide both username and password") {
          this.setState({
            error: "Please provide both username and password"
          })
        } else {
          localStorage.setItem('token', response.token)
          window.location.assign('/faqs/')
        }
      })
  }

  handleLogout(e){
    e.preventDefault()
    localStorage.removeItem('token')
    window.location.assign('/faqs/')
  }

  render(){
    return(
      <div id="login-page">
        <h2>Login</h2>
        {localStorage.getItem('token') !== null ?
          <div>
            <p>You are logged in</p>
            <form onSubmit={this.handleLogout}>
              <button className="btn delete modal-action modal-close right">
                Logout
              </button>
            </form>
          </div>
        :
          <div>
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                {this.state.error !== '' && <h5 className="error-message">{this.state.error}</h5>}
                <div className="input-field col s5">
                  <input id="username" type="text" className="validate" name="username" />
                  <label htmlFor="username">Username</label>
                </div>
              </div>
              <div className="row">
                <div className="input-field col s5">
                  <input id="password" type="password" className="validate" name="password" />
                  <label htmlFor="password">Password</label>
                </div>
              </div>
              <button className="btn confirm modal-action modal-close right">
                Login
              </button>
            </form>
          </div>
        }
      </div>
    )
  }
}

export default Login
