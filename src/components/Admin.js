import React from 'react'
import axios from 'axios'
import _ from 'lodash'
import { Link } from 'react-router-dom'
import RecentlyModified from './RecentlyModified'

class Admin extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      unpublishedProducts: [],
      unpublishedBulks: [],
      unpublishedIngredients: [],
      unpublishedStandards: []
    }
    this.getUnpublishedProducts()
    this.getUnpublishedBulks()
    this.getUnpublishedIngredients()
    this.getUnpublishedStandards()
  }

  getUnpublishedProducts() {
    // axios.get('https://localhost:4430/faq/product/')
    axios.get('/faq/product/')
      .then(res => {
        const unpublishedProducts = []
        var i
        for (i = 0; i < res.data.length; i++) {
          if (res.data[i].published === false){
            unpublishedProducts.push(res.data[i])
          }
        }
        const sortedProducts = _.orderBy(unpublishedProducts, [product => product.description], ['asc']);
        this.setState({
          unpublishedProducts: sortedProducts
        })
      })
  }

  getUnpublishedStandards() {
    // axios.get('https://localhost:4430/faq/standard/')
    axios.get('/faq/standard/')
      .then(res => {
        const unpublishedStandards = []
        var i
        for (i = 0; i < res.data.length; i++) {
          if (res.data[i].published === false){
            unpublishedStandards.push(res.data[i])
          }
        }
        const sortedStandards = _.orderBy(unpublishedStandards, [standard => standard.name], ['asc']);
        this.setState({
          unpublishedStandards: sortedStandards
        })
      })
  }

  getUnpublishedIngredients() {
    // axios.get('https://localhost:4430/faq/ingredient/')
    axios.get('/faq/ingredient/')
      .then(res => {
        const unpublishedIngredients = []
        var i
        for (i = 0; i < res.data.length; i++) {
          if (res.data[i].published === false){
            unpublishedIngredients.push(res.data[i])
          }
        }
        const sortedIngredients = _.orderBy(unpublishedIngredients, [ingredient => ingredient.description], ['asc']);
        this.setState({
          unpublishedIngredients: sortedIngredients
        })
      })
  }

  getUnpublishedBulks() {
    // axios.get('https://localhost:4430/faq/bulk/')
    axios.get('/faq/bulk/')
      .then(res => {
        const unpublishedBulks = []
        var i
        for (i = 0; i < res.data.length; i++) {
          if (res.data[i].published === false){
            unpublishedBulks.push(res.data[i])
          }
        }
        const sortedBulks = _.orderBy(unpublishedBulks, [bulk => bulk.name], ['asc']);
        this.setState({
          unpublishedBulks: sortedBulks
        })
      })
  }

  render() {
    return (
      <div id="admin-page">
        <div className="row">
          <div className="col m12 s12">
            <h2>Admin </h2>
            <p><i>click below to jump to category</i></p>
            <ul className="admin-ul">
              <li><a href="#recently-modifed">Recently Modified</a></li>
              <li><a href="#unpublished-products">Unpublished Products</a></li>
              <li><a href="#unpublished-bulks">Unpublished Bulks</a></li>
              <li><a href="#unpublished-raw-materials">Unpublished Raw Materials</a></li>
              <li><a href="#unpublished-nordic-standards">Unpublished Nordic Standards</a></li>
            </ul>
            <br/>
            <br/>
          </div>
        </div>

        <div className="row">
          <div className="col m12 s12" id="recently-modified">
            <RecentlyModified />
          </div>
        </div>

        <div className="row">
          <div className="col m12 s12">
            <b id="unpublished-products">Unpublished Products</b>
              {this.state.unpublishedProducts.map(prod => {
                let url = "/faqs/product/" + prod.id
                return <p key={prod.id}><Link to={url}>{prod.description}</Link></p>
              })}
          </div>
        </div>

        <div className="row">
          <div className="col m12 s12">
            <b id="unpublished-bulks">Unpublished Bulks</b>
            {this.state.unpublishedBulks.map(bulk => {
              let url = "/faqs/bulk/" + bulk.id
              return <p key={bulk.id}><Link to={url}>{bulk.name}</Link></p>
            })}
          </div>
        </div>

        <div className="row">
          <div className="col m12 s12">
            <b id="unpublished-raw-materials">Unpublished Raw Materials</b>
            {this.state.unpublishedIngredients.map(ingredient => {
              let url = "/faqs/ingredient/" + ingredient.id
              return <p key={ingredient.id}><Link to={url}>{ingredient.description}</Link></p>
            })}
          </div>
        </div>

        <div className="row">
          <div className="col m12 s12">
            <b id="unpublished-nordic-standards">Unpublished Nordic Standards</b>
            {this.state.unpublishedStandards.map(ns => {
              let url = "/faqs/nordic-standard/" + ns.id
              return <p key={ns.id}><Link to={url}>{ns.name}</Link></p>
            })}
          </div>
        </div>
      </div>
    )
  }
}

export default Admin
