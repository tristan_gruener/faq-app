import React from 'react'
import { Modal } from 'react-materialize'
import FaqList from './FaqList'
import { Link } from 'react-router-dom'

class NordicStandardPage extends React.Component {
  constructor(){
    super()
    this.state = {
      name: '',
      description: '',
      standard: ''
    }
  }

  componentDidMount() {
    this.init()
  }

  componentDidUpdate(prevProps, prevState, snapshot){
    if(this.props.match.params.standardId !== prevProps.match.params.standardId){
      this.init()
    }
  }

  init(){
    const urlId = this.props.location.pathname.split('/')[3]
    // dev
    // fetch(`https://localhost:4430/faq/standard/${urlId}/`)
    // prod
    fetch(`/faq/standard/${urlId}/`)
      .then((res) => res.json())
      .then((json) => {
        const name = json.name
        const description = json.description
        const standard = json.standard
        const featured = json.featured
        const featuredHeadline = json.featured_headline

        this.setState({
          name,
          description,
          standard,
          id: urlId,
          published: json.published,
          featured,
          featuredHeadline
        })
      }
    );
  }



  render(){
    return(
      <div id="nordic-standard-page">
        {/* <div className="col"> */}
        <div className="col s12">
          <h2>{this.state.name}</h2>
          <p><b>Description:</b> {this.state.description}</p>
          <p dangerouslySetInnerHTML={{__html: this.state.standard}}/>

          {localStorage.getItem('isLoggedIn') === "true" &&
            <Link
              to={{
                pathname: "/faqs/add-nordic-standard",
                state: {
                  context: "Edit",
                  pastRef: this.props.location.pathname,
                  id: this.state.id,
                  name: this.state.name,
                  description: this.state.description,
                  standard: this.state.standard,
                  published: this.state.published,
                  featured: this.state.featured,
                  featuredHeadline: this.state.featuredHeadline
              }}}
            >
              <span className="waves-effect waves-light btn right">Edit Nordic Standard</span>
            </Link>
          }
        </div>
      </div>
    )
  }
}

export default NordicStandardPage
