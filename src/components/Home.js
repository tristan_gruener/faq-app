import React from 'react'
import Search from './Search'
import FeaturedUpdates from './FeaturedUpdates'

const Home = () => (
  <div id="content-wrapper" className="home-page">
    <Search />
    <FeaturedUpdates />
  </div>
)

export default Home
