import React from 'react'
import { Link } from 'react-router-dom'

class Header extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      isLoggedIn: false
    }
  }

  componentDidMount(){
    this.authenticate()
  }

  authenticate() {
    switch (localStorage.getItem('token')) {
      case "undefined":
        // console.log('text string undefined')
        localStorage.setItem('isLoggedIn', "false")
        break;
      case null:
        // console.log('js keyword null')
        localStorage.setItem('isLoggedIn', "false")
        break;
      case "null":
        // console.log('js keyword null')
        localStorage.setItem('isLoggedIn', "false")
        break;
      case undefined:
        // console.log('js keyword undefined')
        localStorage.setItem('isLoggedIn', "false")
        break;
      default:
        // console.log('default case, logged in')
        this.setState({
          isLoggedIn: true
        })
        localStorage.setItem('isLoggedIn', "true")
    }
  }

  render() {
    return (
      <nav style={{
        "backgroundColor":"#fafafa"
      }}>
        <div className="nav-wrapper">
          <a href="/faqs/">
            <img
              src="https://www.nordicnaturals.com/static/version1539195396/frontend/NordicNaturals/default/en_US/images/logo.png"
              alt="NordicNaturalsLogo"
              style={{
                "width": "140px",
                "marginTop": "10px",
                "marginLeft": "8px"
              }}
            />
          </a>
          <a href="/faqs/"
            style={{
              "color":"gray",
              "fontSize": "16px",
              "marginLeft": "14px",
              "position": "absolute"
            }}
          >
            <span style={{"paddingBottom": "14px"}}>Customer Service FAQs</span>
          </a>

          <ul id="nav-mobile" className="right">
            {localStorage.getItem('token') !== null ?
              <div>
                <li>
                  <Link to={"/faqs/add-product"} style={{"color":"#0277bd"}}>
                    <i className="fas fa-plus" style={{"fontSize": "12px", "height": "0px"}}></i> Product
                  </Link>
                </li>
                <li>
                  <Link to={"/faqs/add-bulk"} style={{"color":"#0277bd"}}>
                    <i className="fas fa-plus" style={{"fontSize": "12px", "height": "0px"}}></i> Bulk
                  </Link>
                </li>
                <li>
                  <Link to={"/faqs/add-ingredient"} style={{"color":"#0277bd"}}>
                    <i className="fas fa-plus" style={{"fontSize": "12px", "height": "0px"}}></i> Raw Material
                  </Link>
                </li>
                <li>
                  <Link to={"/faqs/add-nordic-standard"} style={{"color":"#0277bd"}}>
                    <i className="fas fa-plus" style={{"fontSize": "12px", "height": "0px"}}></i> Nordic Standard
                  </Link>
                </li>
                <li>
                  <Link to={"/faqs/admin"} style={{"color":"#0277bd"}}>
                    <i className="fas fa-coffee" style={{"fontSize": "12px", "height": "0px"}}></i> Admin
                  </Link>
                </li>
                |
                <li>
                  <Link to={"/faqs/login"} style={{"color":"#0277bd"}}>
                    <i className="fas fa-user" style={{"height": "0px", "fontSize": "16px"}}></i>
                  </Link>
                </li>
                <li>
                  <Link to={"/faqs"} style={{"color":"#0277bd"}}>
                    <i className="fas fa-search" style={{"height": "0px", "fontSize": "16px"}}></i><small> Search</small>
                  </Link>
                </li>
              </div>
            :
              <div>
                <li>
                  <Link to={"/faqs"} style={{"color":"#0277bd"}}>
                    <i className="fas fa-search" style={{"height": "0px", "fontSize": "16px"}}></i><small> Search</small>
                  </Link>
                </li>
                <li>
                  <Link to={"/faqs/login"} style={{"color":"#0277bd"}}>
                    <i className="fas fa-user" style={{"height": "0px", "fontSize": "16px"}}></i><small> log in</small>
                  </Link>
                </li>
              </div>
            }
          </ul>
        </div>
      </nav>
    )
  }
}

export default Header
