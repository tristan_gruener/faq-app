import React from 'react'
import { Link } from 'react-router-dom'
import { Modal } from 'react-materialize'
import FaqList from './FaqList'

class BulkPage extends React.Component{
  constructor(props) {
    super(props)
    this.getProductsContainingBulk = this.getProductsContainingBulk.bind(this)
    this.state = {
      name: 'placeholder',
      productsInBulk: [{}],
      renderProductsInBulk: false
    }
  }

  componentDidMount() {
    this.init()
  }

  componentDidUpdate(prevProps, prevState, snapshot){
    if(this.props.match.params.bulkId !== prevProps.match.params.bulkId){
      this.init()
    }
  }

  init(){
    let bulkHasInfoPage = false
    let needsInfoPageOnEdit = false
    let infoPageId = ''
    const urlId = this.props.location.pathname.split('/')[3]

    fetch(`/faq/bulk/${urlId}/`).then((res) => res.json()).then((json) => {
    // fetch(`https://localhost:4430/faq/bulk/${urlId}/`).then((res) => res.json()).then((json) => {
      this.getProductsContainingBulk(json.id)
      const rawMaterialsInBulkRender = []

      json.ingredients.map(rm => {
        const rmId = rm.split('/')[5]
        rawMaterialsInBulkRender.push(localStorage.getItem(rmId))
      })

      const bulkInfo = JSON.stringify(json)
      //check if info page is present
      try {
        infoPageId = JSON.parse(bulkInfo).info_pages[0].id
        bulkHasInfoPage = true
      } catch(error){
        needsInfoPageOnEdit = true
      }

      if(bulkHasInfoPage) {
        const infoPages = JSON.parse(JSON.parse(bulkInfo).info_pages[0].html_j)
        const manufacturing = infoPages.manufacturing
        const allergens = infoPages.allergens

        this.setState({
          name: json.name,
          id: json.id,
          infoPageId: infoPageId,
          bulkImage: json.image_urls[0],
          needsInfoPageOnEdit: needsInfoPageOnEdit,
          shelfLife: manufacturing.shelfLife,
          manufactureLocation: manufacturing.manufactureLocation,
          certifications: manufacturing.certifications,
          manufactureWarning: parseInt(allergens.manufactureWarning),
          manufactureWarningRender: this.setManufactureWarning(parseInt(allergens.manufactureWarning)),
          milkAllergen: allergens.milkAllergen,
          eggAllergen: allergens.eggAllergen,
          soyAllergen: allergens.soyAllergen,
          fishAllergen: allergens.fishAllergen,
          shellFishAllergen: allergens.shellFishAllergen,
          wheatAllergen: allergens.wheatAllergen,
          treeNutsAllergen: allergens.treeNutsAllergen,
          peanutsAllergen: allergens.peanutsAllergen,
          faqs: json.faqs,
          published: json.published,
          bulkType: json.bulk_type === null ? 'undefined' : json.bulk_type,
          rawMaterialsInBulk: json.ingredients,
          rawMaterialsInBulkRender: rawMaterialsInBulkRender,
          featured: json.featured,
          featuredHeadline: json.featured_headline
          // productsInBulk: []
        })
      } else {
        this.setState({
          name: json.name,
          bulkType: json.bulk_type === null ? 'undefined' : json.bulk_type,
          id: json.id,
          infoPageId: '',
          needsInfoPageOnEdit: needsInfoPageOnEdit,
          shelfLife: '',
          bulkImage: 'no image here...',
          manufactureLocation: '',
          certifications:'',
          manufactureWarning: 0,
          milkAllergen: false,
          eggAllergen: false,
          soyAllergen: false,
          fishAllergen: false,
          shellFishAllergen: false,
          wheatAllergen: false,
          treeNutsAllergen: false,
          peanutsAllergen: false,
          faqs: [],
          rawMaterialsInBulk: json.ingredients,
          rawMaterialsInBulkRender: rawMaterialsInBulkRender,
          featured: json.featured,
          featuredHeadline: json.featured_headline
          // productsInBulk: []
        })
      }

    })
    window.scrollTo(0,0)
  }

  setManufactureWarning(val) {
    var wrng = ''
    switch (val){
      case 0:
        wrng =
          <span style={{ "color":"#4caf50" }}>
            <i class="fas fa-thumbs-up"></i> This product is manufactured in a facility free of Soy, Dairy, Wheat, Rye, Barley, and Nuts
          </span>
        break;
      case 1:
        wrng =
          <span style={{ "color":"#d81b60" }}>
            <i class="fas fa-exclamation-triangle"></i> This product is manufactured in a facility that processes Soy, Dairy, Wheat, Rye, Barley, or Nuts
          </span>
        break;
      case 2:
        wrng = ''
    }
    return wrng
  }

  getProductsContainingBulk(bulkId) {
    let publishedProducts = []
    let productsInBulk = []

    // fetch(`https://localhost:4430/faq/product/`).then((res) => res.json()).then((json) => {
    fetch(`/faq/product/`).then((res) => res.json()).then((json) => {
      json.map((product) => {
        if(product.published){
          publishedProducts.push(product)
        }
      })

      publishedProducts.map((publishedProduct) => {
        // fetch(`https://localhost:4430/faq/product/${publishedProduct.id}`).then((res) => res.json()).then((json) => {
        fetch(`/faq/product/${publishedProduct.id}`).then((res) => res.json()).then((json) => {
          const bulkUrl = json.bulk
          const bulkUrlId = parseInt(bulkUrl.split('/')[5], 10)
          if(bulkUrlId === bulkId) {
            productsInBulk.push(json)
          }
        }).then(() => {
          this.setState({
            productsInBulk,
            renderProductsInBulk: true
          })
        })
      })
    })
  }

  render(){
    return(
      <div id="bulk-page">
        <div>
          <h2>{this.state.name}</h2>
          <div id="image-area">
            {this.state.bulkImage !== undefined &&
              <div>
                <Modal
                  header={this.state.name}
                  fixedFooter
                  trigger=
                    {<span>
                      <img
                        className="thumbnail z-depth-1"
                        alt="bulk.bulk_image_thumbnail"
                        src={this.state.bulkImage}
                        style={{'width': '200px', 'cursor':'pointer'}}
                      />
                    </span>}
                >
                  <img
                    alt="bulk.bulk_image"
                    src={this.state.bulkImage}
                    style={{'width': '500px'}}
                  />
                </Modal>
              </div>
            }
          </div>

          <h4>Manufacturing</h4>

          <p>
            <b>Shelf Life:</b> {this.state.shelfLife}
            <br/>
            <b>Manufacturing Location:</b> {this.state.manufactureLocation}
            <br/>
            <b>Certifications:</b> {this.state.certifications}
          </p>

          <h4>Raw Materials In Bulk</h4>

          {this.state.rawMaterialsInBulk &&
            this.state.rawMaterialsInBulk.map((rm, i) => {
            const arr = rm.split('/')
            const pos = arr.length - 2
            return <p><Link to={`/faqs/ingredient/${arr[pos]}/`}>{this.state.rawMaterialsInBulkRender[i]}</Link></p>
          })}

          <h4>Products Made From Bulk</h4>

          {this.state.productsInBulk.map((product) => (
            <p key={product.id}><Link to={`/faqs/product/${product.id}/`}>{product.description}</Link></p>
          ))}

          <h4>Allergens</h4>

          <b>{this.state.manufactureWarningRender}</b>

          <table>
            <thead>
              <tr>
                <th>Milk</th>
                <th>Eggs</th>
                <th>Soy</th>
                <th>Fish</th>
                <th>Shellfish</th>
                <th>Wheat</th>
                <th>Treenuts</th>
                <th>Peanuts</th>
              </tr>
            </thead>

            <tbody>
              <tr>
                <td>{this.state.milkAllergen && <i class="fas fa-check"></i>}</td>
                <td>{this.state.eggAllergen && <i class="fas fa-check"></i>}</td>
                <td>{this.state.soyAllergen && <i class="fas fa-check"></i>}</td>
                <td>{this.state.fishAllergen && <i class="fas fa-check"></i>}</td>
                <td>{this.state.shellFishAllergen && <i class="fas fa-check"></i>}</td>
                <td>{this.state.wheatAllergen && <i class="fas fa-check"></i>}</td>
                <td>{this.state.treeNutsAllergen && <i class="fas fa-check"></i>}</td>
                <td>{this.state.peanutsAllergen && <i class="fas fa-check"></i>}</td>
              </tr>
            </tbody>
          </table>
          {localStorage.getItem('isLoggedIn') === "true" &&
            <Link
              to={{
                pathname: "/faqs/add-bulk",
                state: {
                  category: "Allergens",
                  context: "Edit",
                  needsInfoPageOnEdit: this.state.needsInfoPageOnEdit,
                  id: this.state.id,
                  name: this.state.name,
                  bulkImage: this.state.bulkImage,
                  shelfLife: this.state.shelfLife,
                  infoPageId: this.state.infoPageId,
                  manufactureLocation: this.state.manufactureLocation,
                  certifications: this.state.certifications,
                  manufactureWarning: this.state.manufactureWarning,
                  milkAllergen: this.state.milkAllergen,
                  eggAllergen: this.state.eggAllergen,
                  soyAllergen: this.state.soyAllergen,
                  fishAllergen: this.state.fishAllergen,
                  shellFishAllergen: this.state.shellFishAllergen,
                  wheatAllergen: this.state.wheatAllergen,
                  treeNutsAllergen: this.state.treeNutsAllergen,
                  peanutsAllergen: this.state.peanutsAllergen,
                  bulkType: this.state.bulkType,
                  published: this.state.published,
                  rawMaterialsInBulk: this.state.rawMaterialsInBulk,
                  rawMaterialsInBulkRender: this.state.rawMaterialsInBulkRender,
                  featuredHeadline: this.state.featuredHeadline,
                  published: this.state.published
              }}}
            >
              <span className="waves-effect waves-light btn right">Edit Bulk</span>
            </Link>
          }

          <div className="section-heading">
            <h4>FAQs</h4>
            {this.state.faqs !== undefined &&
              <FaqList
                faqs={this.state.faqs}
                pastRef={this.props.location.pathname}
                nameRef={this.state.name}
                prodIdRef={this.state.id}
              />
            }
            {localStorage.getItem('isLoggedIn') === "true" &&
              <Link
                to={{
                  pathname: "/faqs/add-faq",
                  state: {
                    category: "generalInformation",
                    objectType: "bulk",
                    nameRef: this.state.name,
                    pastRef: this.props.location.pathname,
                    prodIdRef: this.state.id
                  }
                }}
              >
                <span className="waves-effect waves-light btn right">Add Faq</span>
              </Link>
            }
          </div>
        </div>
      </div>
    )
  }
}

export default BulkPage
