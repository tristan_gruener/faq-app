import React from 'react'
import Faq from './Faq'

const FaqList = (props) => (
  <div>
    {props.faqs.map(faq => (
      <Faq
        {...faq}
        key={faq.id}
        prodIdRef={props.prodIdRef}
        pastRef={props.pastRef}
        nameRef={props.nameRef}
        objectType={props.objectType}
      />
    ))}
  </div>
)

export default FaqList
