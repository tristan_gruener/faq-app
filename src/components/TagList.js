import React from 'react'
import Tag from './Tag'

const TagList = ({tags}) => (
  <div>
    {tags.map(tag => (
      <Tag tag={tag} key={tag}/>
    ))}
  </div>
)

export default TagList
