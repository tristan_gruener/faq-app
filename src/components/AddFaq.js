import React from 'react'
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import '../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

class AddFaq extends React.Component {
  constructor(props) {
    super(props)
    this.startAddInfo = this.startAddInfo.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    const html = '<p>Hey this <strong>editor</strong> rocks 😀</p>'
    const contentBlock = htmlToDraft(html)
    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks)
      const editorState = EditorState.createWithContent(contentState)
      this.state = {
        editorState,
        context: '',
        question: ''
      }
    }

    if(this.props.location.state.context === 'edit'){
      this.getEditableContent()
    }
  }

  startAddInfo(editorState){
    const question = document.getElementById('question').value
    const answer = editorState
    const category = this.props.location.state.category
    const prodUrlId = this.props.location.state.prodIdRef

    if(this.props.location.state.context !== 'edit'){
      //add state
      const method = 'PUT'
      const url = `/faq/${this.props.location.state.objectType}/${this.props.location.state.prodIdRef}/`
      const data = {
        'faqs': [
          {
            'page_section': category,
            'question': question,
            'answer': answer
          }
        ],
        'info_pages': []
      }

      this.pushToDb(url, method, data, prodUrlId)
    } else {
      //edit state
      const method = 'PUT'
      const url = `/faq/faq/${this.props.location.state.id}/`
      const data = {
        'question': question,
        'answer': answer
      }
      this.pushToDb(url, method, data, prodUrlId)
    }
  }


  pushToDb(url, method, data, redirect){
    fetch(url, {
      method: method,
      body: JSON.stringify(data),
      headers: {
        'Authorization': ' Token ' + localStorage.getItem('token'),
        'Content-Type': 'application/json'
      }
    }).then(res => res.json()).then(response => {
      //TK
      alert('Success')
      const url = `/faqs/${this.props.location.state.objectType}/${this.props.location.state.prodIdRef}/`
      this.props.history.push(url)
      window.scrollTo(0,0)
    })
  }

  handleDelete(){
    const url = `/faq/faq/${this.props.location.state.id}/`
    const prodUrlId = this.props.location.state.prodIdRef
    const deleteConfirmation = window.confirm("Are you sure you want to delete this faq?");
    if (deleteConfirmation) {
      fetch(url, {
        method: 'DELETE',
        headers: {
          'Authorization': ' Token ' + localStorage.getItem('token'),
          'Content-Type': 'application/json'
        }
      }).then(() => {
        // this.props.history.push(`/${this.props.location.state.objectType}/${prodUrlId}`)
        const url = `/faqs/${this.props.location.state.objectType}/${this.props.location.state.prodIdRef}/`
        this.props.history.push(url)
        window.scrollTo(0,0)
      })
    }
  }

  onEditorStateChange: Function = (editorState) => {
    this.setState({
      editorState,
    })
  }

  updateQuestionValue(evt){
    this.setState({
      question: evt.target.value
    });
  }

  getEditableContent(){
    const question = this.props.location.state.question
    const html = this.props.location.state.answer
    const contentBlock = htmlToDraft(html)
    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks)
      const editorState = EditorState.createWithContent(contentState);
      this.state = {
        editorState,
        question
      }
    }
  }

  render() {
    const { editorState } = this.state;
    return (
      <div>
        <h4>
          {
            this.props.location.state.context === "edit" ?
              `Edit FAQ for ${this.props.location.state.nameRef}`
                :
              `Add FAQ to ${this.props.location.state.nameRef}`
          }
        </h4>
        <form>
          <div className="col s12">
            <div className="input-field">
              <input
                id="question"
                type="text"
                className="validate"
                ref="question"
                value={this.state.question}
                onChange={evt => this.updateQuestionValue(evt)}/>
                <label
                  htmlFor="question"
                  id="question-field"
                  className={this.props.location.state.context === "edit" && "active"}
                >
                  Question
                </label>
            </div>
          </div>
        </form>

        <Editor
          editorState={editorState}
          onEditorStateChange={this.onEditorStateChange}
          wrapperClassName="wrapper-class"
          editorClassName="editor-class"
          toolbarClassName="toolbar-class"
          toolbar={{
            inline: { inDropdown: true },
            list: { inDropdown: true },
            textAlign: { inDropdown: true },
            link: { inDropdown: true },
            history: { inDropdown: true },
            font: { inDropdown: false }
          }}
          editorStyle={{
            "border": "1px solid #F1F1F1",
            "height": "320px",
            "maxHeight": "100%",
            "marginBottom": "20px",
            "paddingLeft": "18px",
            "paddingRight": "18px"
          }}
        />

        <button
          className="btn confirm modal-action modal-close right"
          type="button"
          onClick={() => { this.startAddInfo(draftToHtml(convertToRaw(editorState.getCurrentContent()))) }}
        >
          {this.props.location.state.context === "edit" ? "Edit" : "Add"}
        </button>
        <br/>
        <br/>
        <br/>
        {this.props.location.state.context === "edit" &&
          <button
            className="btn delete modal-action modal-close right"
            type="button"
            onClick={this.handleDelete}
          >
            Delete
          </button>
        }
      </div>
    );
  }
}

export default AddFaq
