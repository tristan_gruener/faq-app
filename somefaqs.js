{/* <Faq
  question="Will taking a melatonin supplement stop my body from producing its own melatonin?"
  answer="No. According to current research, taking supplemental melatonin does not appear to down regulate endogenous"
/>

<Faq
  question="Are these gummies soft chew or 'harder' gummies?"
  answer="They're very soft."
/>

<Faq
  question="How long does it take to leave your system?"
  answer="TBD."
/>

<Faq
  question="Most melatonin products on the market offer 3-5 mg per serving. why does this product offer a lower dose at 1.5 mg per gummy?"
  answer="Most clinical studies categorize amounts of 3mg and higher as a high dose. While many supplements on the market offer extremely high doses of melatonin (averaging at 5 mg), Nordic Naturals provides the adequate amount of melatonin needed to achieve a restful sleep. For most people, higher doses of melatonin offer no advantages for sleep and can lead to higher plasma melatonin levels the next day, causing morning drowsiness.There are anecdotal reports that high melatonin also leads to night waking and vivid dreams. Its is recommended to start with a small dose and gradually increase until the desired effect is achieved. SHARON: ADD FOLLOW THE SIGNS BOX ON SELL SHEET"
/>

<Faq
  question="Does this product need to be refrigerated?"
  answer="Store in a cool, dry place away from sunlight. No refrigeration required."
/>

<Faq
  question="Will the gummies harden after the bottle has been opened?"
  answer="TBD.

Kayla- I've kept five, single Melatonin gummies on my desk (out of bottle) for a couple weeks and they have gotten powdery white, shrank in size, and hardened. Currently conducting same experiment with Curcumin Gummies."
/>

<Faq
  question="Will the appearance or color of gummies change over time?"
  answer="TBD."
/>

<Faq
  question="Are these considered sleeping pills?"
  answer="Sleeping pills are classified under a category of drugs known as sedative-hypnotics. Melatonin is a dietary supplement and is not considered a sleeping pill."
/>

<Faq
  question="Will this help with jet lag?"
  answer="Yes. Supplementing with melatonin has been shown to effectively promote sleep in travelers with jet lag."
/>

<Faq
  question="Is this product usp or nsf verified?"
  answer="TBD."
/>


<h4>Safety & Dosage</h4>

<Faq
  question="Does this product contain sugar?"
  answer="No."
/>

<Faq
  question="What is a sugar alcohol?"
  answer="Sugar alcohols (polyols) are found naturally in fruits and vegetables, but they can also be manufactured. Because

they are structurally similar to sugar they are able to activate sweet taste receptors on the tongue, yet they contain

fewer calories than sugar. Xylitol has a very low glycemic index (12), compared to table sugar (sucrose), which has

a glycemic index of 65. The glycemic index (GI) ranks carbohydrates in foods according to how they affect blood

glucose levels. Carbohydrates with a low GI value (55 or less) are more slowly digested, absorbed, and metabolized,

and cause a lower and slower rise in blood glucose and insulin levels. In contrast, artificial sweeteners like aspartame,

sucralose, and saccharin are synthetic (lab invented) sugar substitutes that have glycemic index values of zero."
/>

<Faq
  question="Can I give this to my pet to help it sleep better?"
  answer="Kayla- No!
According to http://www.vcahospitals.com/main/pet-health-information/article/animal-health/xylitol-toxicity-in-dogs/4340, Xylitol causes a rapid release of insulin in dogs, which can quickly lead to a crash in blood sugar. Regardless of whether or not your dog has a history of hypoglycemia (low blood sugar), you will want to contact a veterinarian right away if your pet consumes xylitol. The toxicity threshold for xylitol in dogs is 50mg per pound of body weight. This amount ingested can lead to liver failure."
/>

<Faq
  question="Are these non-habit forming?"
  answer="Kayla- Since these are supplements, they are non-addicting and non-habit forming."
/>

<Faq
  question="Will I still wake up if my baby is crying or if there is a loud noise?"
  answer="Kayla- This product is not a sedative or a tranquilizer and is not meant to mimic a drug which induces drowsiness...."
/>

<Faq
  question="What would be the serving size of our Melatonin Gummies for kids (age not specified)?"
  answer="Our Melatonin Gummies were formulated for adults.  They are not recommended for children. AS NEEDED for KIDS:  In studies of children with chronic sleep initiation children were given the following milligrams for a 2 month period; 2-6 years were given 1.4mg; children 7-11 were given 2mg and children 12-18 were given 2.8mg.  91% of the children showed improvement or complete resolution of various sleep issues. The children were all under a doctor’s supervision. (no source cited -kayla)"
/>

<Faq
  question="Can this gummy be broken in half if needed?"
  answer="Kayla- this does not guarantee exactly half a dose..."
/>

<Faq
  question="Is this product safe for children?"
  answer="Melatonin has been tested clinically in premature and full-term  neonates, children, teens, adults, and the elderly, and appears to be safe for people of all ages and health states."
/>

<Faq
  question="Are there any side effects of melatonin?"
  answer="There are no side effects of melatonin when supplementing with the appropriate dose. Taking a higher dose or at a later time than recommended may cause morning drowsiness.  Studies have shown doses up to 40mg have no significant side effects besides morning drowsiness."
/>

<Faq
  question="Is melatonin safe to take with alcohol?"
  answer="It is not recommended to take melatonin while drinking, as alcohol can interfere with its effectiveness and may cause adverse effects. "
/>

<Faq
  question="Are there any medication interactions with this product?"
  answer="If you are taking medications, please consult your physician before using this product."
/>

<Faq
  question="Will taking a higher dose make me sleep better?"
  answer="Taking higher doses than necessary offers no advantages for sleep, and can lead to morning drowsiness. Start with a low dose and increase in increments of 0.5 mg if sleep problems persist.--Maybe include mention of vivid dreams occurring with higher doses."
/>

<Faq
  question="When should i take this product?"
  answer="It is recommended to take melatonin 4-6 hours before desired sleep onset for maximum advance."
/>

<Faq
  question="Do i need to take this product everyday?"
  answer="Melatonin can be taken if you are experiencing a lack of sleep and abnormal sleep patterns. It is not necessary to take melatonin everyday."
/>

<Faq
  question="Can i take this during the day for anxiety?"
  answer="Melatonin should only be taken before going to bed. Since insomnia can be a common side effect of anxiety, taking melatonin at night time can help individuals struggling with anxiety achieve a more restful sleep.

SHARON: I’d be a bit more specific here—Melatonin isn’t an anxiety medication. It may help with symptoms, such as insomnia, associated with anxiety, but it is not intended to make someone feel less anxious.)

Wording option: Melatonin is not an anxiety medication, nor is it intended to make someone feel less anxious. It may help with symptoms associated with anxiety, such as insomnia."
/> */}
